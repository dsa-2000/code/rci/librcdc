// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "rcpdc.hpp"

#include <algorithm>
#include <future>
#include <map>
#include <memory>
#include <ranges>
#include <variant>
#include <vector>

/*! \brief helper class for use in std::visit() */
template <class... Ts>
struct overloaded : Ts... {
  using Ts::operator()...;
};
template <class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;

namespace rcp::dc {

/*! BlockFiller class
 *
 * \tparam P fully specialized FPacket class
 * \tparam B fully specialized FBlock class
 * \tparam FillImpl copy packet to block implementation function
 *
 * Note the constraints on relations of members of P and B. Although I suspect
 * none of these constraints is strictly necessary, they simplify the range of
 * cases that are required to be supported by this class (and its sub-classes),
 * and, as requirements, document those restrictions.
 *
 * The filler parameter is typically inherited from a DataCapture
 * specialization. See DataCapture documentation for a description of the type.
 */
template <IsFPacket P, IsFBlock B, typename FillImpl>
  requires(P::num_channels == B::num_channels)
          and (P::num_polarizations == B::num_polarizations)
          and (P::sample_interval_ns == B::sample_interval_ns)
class BlockFiller {

public:
  using packet_type = P;
  using packet_type_p = packet_type*;
  using block_type = B;
  using block_type_sp = std::shared_ptr<block_type>;
  using block_type_p = block_type*;
  using fill_impl_t = FillImpl;
  using fill_result_t = std::tuple<std::size_t, block_type_sp>;

private:
  struct BlkKey {
    // NOLINTBEGIN(misc-non-private-member-variables-in-classes)
    timestamp_type timestamp_offset{};
    channel_type channel_offset{};
    // NOLINTEND(misc-non-private-member-variables-in-classes)

    // NOLINTBEGIN(bugprone-easily-swappable-parameters)
    BlkKey(
      timestamp_type block_duration,
      timestamp_type epoch,
      timestamp_type ts,
      channel_type ch)
      : timestamp_offset(round_down_ts(block_duration, epoch, ts))
      , channel_offset(ch) {}
    // NOLINTEND(bugprone-easily-swappable-parameters)

    BlkKey(
      timestamp_type block_duration,
      timestamp_type const& epoch,
      packet_type const& pkt,
      bool begin = true)
      : BlkKey(
          block_duration,
          epoch,
          pkt.timestamp_offset
            + (begin ? 0 : (packet_type::duration() - packet_type::sample_interval_ns)),
          pkt.channel_offset) {}

    BlkKey(block_type const& blk)
      : timestamp_offset(blk.timestamp_offset)
      , channel_offset(blk.channel_offset) {}

    auto
    operator<=>(BlkKey const& bk) const {
      auto tc = timestamp_offset <=> bk.timestamp_offset;
      if (tc == 0)
        return channel_offset <=> bk.channel_offset;
      return tc;
    }

    static auto
    round_down_ts(
      timestamp_type block_duration,
      timestamp_type epoch,
      timestamp_type ts) -> timestamp_type {
      assert(epoch <= ts);
      return ((ts - epoch) / block_duration) * block_duration + epoch;
    }
  };

  struct IncompleteBlk {
    block_type_sp block;
    std::size_t num_filled;
    std::
      variant<std::promise<fill_result_t>, std::function<void(fill_result_t)>>
        promise;
  };

  unsigned m_max_block_skew_factor;

  std::map<BlkKey, IncompleteBlk> m_blocks;

  timestamp_type m_block_duration;

public:
  BlockFiller() = delete;

  BlockFiller(
    timestep_offset_type num_timesteps,
    unsigned max_packet_arrival_skew_factor = 1)
    : m_max_block_skew_factor(
        (max_packet_arrival_skew_factor * P::num_timesteps
         + (num_timesteps - 1))
        / num_timesteps)
    , m_block_duration(
        num_timesteps * timestamp_type{packet_type::sample_interval_ns}) {

    auto tr = trace();
    assert(packet_type::num_timesteps <= num_timesteps);
  }

  BlockFiller(const BlockFiller&) = delete;

  BlockFiller(BlockFiller&&) noexcept = default;

  virtual ~BlockFiller() {

    auto tr = trace();
    flush();
  }

  auto
  operator=(const BlockFiller&) -> BlockFiller& = delete;

  auto
  operator=(BlockFiller&&) noexcept -> BlockFiller& = default;

  void
  fill(
    block_type_sp& blk,
    std::variant<
      std::promise<fill_result_t>,
      std::function<void(fill_result_t)>>&& promise) {

    auto tr = trace();
    // log().info<timestamp_type>("fill", {"ts", blk->timestamp_offset});
    m_blocks.emplace(BlkKey(*blk), IncompleteBlk{blk, 0, std::move(promise)});
  }

  void
  process_packets(
    timestamp_type const& epoch, std::vector<packet_type_p> const& packets) {

    auto tr = trace();
    if (!packets.empty() && !m_blocks.empty()) {
      timestamp_type latest_ts = 0;
      // bool ack = false;
      for (auto&& packet : packets) {
        // log().info<std::int64_t,std::int64_t>(
        //   "packet", {"ep", epoch}, {"ts", packet->timestamp_offset});
        if (packet->timestamp_offset >= epoch) {
          auto ch_blocks = std::views::filter(m_blocks, [&](auto&& k_v) {
            return k_v.second.block->channel_offset == packet->channel_offset;
          });
          BlkKey key_i(m_block_duration, epoch, *packet, true /*begin*/);
          BlkKey key_f(m_block_duration, epoch, *packet, false /*begin*/);
          auto blk = std::ranges::find_if(
            ch_blocks, [&](auto&& k_v) { return k_v.first <=> key_i == 0; });
          // if (!ack && blk == ch_blocks.end()) {
          //   log().info<std::int64_t>(
          //     "no matching block",
          //     {"ts", packet->timestamp_offset});
          //   ack = true;
          // }
          // log().info<std::size_t,bool>(
          //   "blk",
          //   {"nblk", m_blocks.size()},
          //   {"end", blk == ch_blocks.end()});
          auto ti = 0;
          while (blk != ch_blocks.end()
                 && BlkKey(*blk->second.block) <= key_f) {
            auto& [k, v] = *blk;
            auto ts_i =
              packet->timestamp_offset + ti * packet_type::sample_interval_ns;
            auto ts_f = std::min(
              ts_i + packet_type::duration(),
              v.block->timestamp_offset + m_block_duration);
            auto num_ts = (ts_f - ts_i) / packet_type::sample_interval_ns;
            // log().info<std::int64_t,std::int64_t,std::int64_t,std::int64_t>(
            //   "process",
            //   {"ts_i", ts_i}, {"ts_f", ts_f}, {"t0",
            //   packet->timestamp_offset},
            //   {"bend", v.block->timestamp_offset + m_block_duration});
            fill_impl_t::copy_packet_to_block(
              num_ts,
              packet->fsamples_mdspan(),
              (ts_i - packet->timestamp_offset)
                / packet_type::sample_interval_ns,
              v.block->samples_mdspan(),
              v.block->weights_mdspan(),
              packet->receiver,
              (ts_i - v.block->timestamp_offset)
                / packet_type::sample_interval_ns);
            v.num_filled += std::size_t(num_ts)
                            * std::size_t(packet_type::num_channels)
                            * std::size_t(packet_type::num_polarizations);
            latest_ts = std::max(v.block->timestamp_offset, latest_ts);
            ti = (ts_f - packet->timestamp_offset)
                 / packet_type::sample_interval_ns;
            ++blk;
          }
        }
      }
      sweep(latest_ts);
    }
  }

  void
  sweep(timestamp_type const& ts) {

    auto tr = trace();
    if (!m_blocks.empty()) {
      auto cutoff_ts = ts - m_max_block_skew_factor * m_block_duration;
      std::vector<BlkKey> finished;
      std::ranges::copy(
        std::views::filter(
          m_blocks,
          [&](auto&& k_v) {
            // if (k_v.first.timestamp_offset < cutoff_ts)
            //   log().info<timestamp_type, timestamp_type>(
            //     "sweep",
            //     {"cutoff_ts", cutoff_ts},
            //     {"ts", k_v.first.timestamp_offset});
            return (k_v.first.timestamp_offset < cutoff_ts)
                   || (k_v.second.num_filled >= k_v.second.block->size());
          })
          | std::views::keys,
        std::back_inserter(finished));
      std::ranges::for_each(finished, [&](auto&& fin_k) {
        auto& iblk = m_blocks.at(fin_k);
        std::visit(
          overloaded{
            [&](std::promise<fill_result_t>& pr) {
              pr.set_value({iblk.num_filled, iblk.block});
            },
            [&](std::function<void(fill_result_t)>& fn) {
              fn({iblk.num_filled, iblk.block});
            }},
          iblk.promise);
        m_blocks.erase(fin_k);
      });
    }
  }

  void
  flush() {

    auto tr = trace();
    for (auto&& k_v : m_blocks) {
      auto& iblk = k_v.second;
      std::visit(
        overloaded{
          [&](std::promise<fill_result_t>& pr) {
            pr.set_value({iblk.num_filled, iblk.block});
          },
          [&](std::function<void(fill_result_t)>& fn) {
            fn({iblk.num_filled, iblk.block});
          }},
        iblk.promise);
    }
    m_blocks.clear();
  }
};

} // end namespace rcp::dc

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
