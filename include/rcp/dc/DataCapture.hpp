// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "BlockFiller.hpp"
#include "rcpdc.hpp"

#include <cassert>
#include <concepts>
#include <functional>
#include <future>
#include <memory_resource>
#include <mutex>
#include <optional>
#include <queue>
#include <thread>
#include <tuple>
#include <variant>

#include <poll.h>
#include <sys/eventfd.h>

/*! \file DataCapture.hpp
 *
 * DataCapture service thread implementation
 */
namespace rcp::dc {

template <class... Ts>
struct overloaded : Ts... {
  using Ts::operator()...;
};

template <typename M>
concept HasSingleBlockMemoryResource =
  std::is_base_of_v<std::pmr::memory_resource, M>
  && M::has_single_block_memory_resource && requires(M mb) {
       { mb.address() } -> std::convertible_to<void*>;
       { mb.size() } -> std::convertible_to<std::size_t>;
     };

class RCPDC_EXPORT single_block_memory_resource
  : public std::pmr::memory_resource {

  void* m_address;

  std::size_t m_size;

  std::unique_ptr<std::pmr::monotonic_buffer_resource> m_buffer;

  std::pmr::memory_resource* m_base_mr;

public:
  static constexpr auto has_single_block_memory_resource = true;

  single_block_memory_resource(void* address, std::size_t size)
    : m_address{address}
    , m_size{size}
    , m_buffer(new std::pmr::monotonic_buffer_resource(
        m_address, m_size, std::pmr::null_memory_resource()))
    , m_base_mr(m_buffer.get()) {

    auto tr = trace();
  }

  template <HasSingleBlockMemoryResource M>
  single_block_memory_resource(M* sbmr)
    : m_address{sbmr->address()}, m_size{sbmr->size()}, m_base_mr(sbmr) {

    auto tr = trace();
  }

  single_block_memory_resource(const single_block_memory_resource&) = delete;

  single_block_memory_resource(single_block_memory_resource&&) = default;

  ~single_block_memory_resource() override = default;

  auto
  operator=(const single_block_memory_resource&)
    -> single_block_memory_resource& = delete;

  auto
  operator=(single_block_memory_resource&&)
    -> single_block_memory_resource& = default;

  auto
  do_allocate(std::size_t bytes, std::size_t alignment) -> void* override {
    auto tr = trace();
    return m_base_mr->allocate(bytes, alignment);
  }

  void
  do_deallocate(void* ptr, std::size_t bytes, std::size_t alignment) override {
    auto tr = trace();
    m_base_mr->deallocate(ptr, bytes, alignment);
  }

  [[nodiscard]] auto
  do_is_equal(std::pmr::memory_resource const& other) const noexcept
    -> bool override {
    auto tr = trace();
    try {
      auto const& other_sbmr =
        dynamic_cast<single_block_memory_resource const&>(other);
      return
        m_base_mr->is_equal(*other_sbmr.m_base_mr)
        && (m_buffer
            || (size() == other_sbmr.size()
                && this->address() == other_sbmr.address()));
    } catch (std::bad_cast const&) {
      return false;
    }
  }

  auto
  address() noexcept -> void* {
    auto tr = trace();
    return m_address;
  }

  [[nodiscard]] auto
  address() const noexcept -> void const* {
    auto tr = trace();
    return m_address;
  }

  [[nodiscard]] auto
  size() const -> std::size_t {
    auto tr = trace();
    return m_size;
  }
};

template <typename Base>
class single_block_pool_mixin : public Base {

  void* m_address;

  std::size_t m_size;

public:
  static constexpr auto has_single_block_memory_resource = true;

  template <HasSingleBlockMemoryResource M>
  single_block_pool_mixin(M* sbmr)
    : Base(sbmr), m_address(sbmr->address()), m_size(sbmr->size()) {

    auto tr = trace();
  }

  auto
  address() noexcept -> void* {
    auto tr = trace();
    return m_address;
  }

  [[nodiscard]] auto
  address() const noexcept -> void const* {
    auto tr = trace();
    return m_address;
  }

  [[nodiscard]] auto
  size() const -> std::size_t {
    auto tr = trace();
    return m_size;
  }
};

using single_block_synchronized_pool =
  single_block_pool_mixin<std::pmr::synchronized_pool_resource>;

using single_block_unsynchronized_pool =
  single_block_pool_mixin<std::pmr::unsynchronized_pool_resource>;

/*! \brief parameters to define sweeping in DataCapture
 *
 * Sweeping is done by generating "ticks" that act as time markers for the
 * DataCapture loop, which are used to complete blocks in the absence of any
 * packets being received. Ticks are generated in an arithmetic sequence with
 * timestamp values
 *
 * DataCapture::m_epoch + i * period
 *
 * whereas the tick is generated when the system time is
 *
 * DataCapture::m_epoch + i * period + delay
 */
struct RCPDC_EXPORT SweepParameters {
  /*! \brief sweep delay
   *
   * must be positive
   */
  std::chrono::milliseconds delay;

  /*! \brief sweep period
   *
   * must be positive
   */
  std::chrono::milliseconds period;
};

/*! \brief DataCapture class
 *
 * \tparam P fully specialized FPacket class
 * \tparam B fully specialized FBlock class
 * \tparam FillImpl template with function as described below
 * \tparam Derived derived class (CRTP)
 *
 * Sub-class Derived must implement the following three member functions
 *
 *    int capture_fd();
 *
 *    std::vector<P*> capture();
 *
 * FillImpl<P, B> must define the function
 *    static void copy_packet_to_block(
 *      typename P::ts_mdsubspan_type const&,
 *      typename B::rcv_ts_mdsubspan_type const&);
 *
 * The file descriptor returned by capture_fd() is used in a call to poll() by
 * DataCapture::run() to provide a signal that new captured packets are
 * ready to fill.
 */
template <
  IsFPacket P,
  IsFBlock B,
  template <typename, typename>
  typename FillImpl,
  typename Derived>
class DataCapture {
public:
  using packet_type = P;
  using packet_type_p = packet_type*;
  using block_type = B;
  using block_type_p = block_type*;
  using block_type_sp = std::shared_ptr<block_type>;

  using fill_result_t = std::tuple<std::size_t, block_type_sp>;

private:
  struct BlkRequest {
    block_type_sp block;
    std::
      variant<std::promise<fill_result_t>, std::function<void(fill_result_t)>>
        promise;
  };
  struct StopRequest {};
  using request_type = std::variant<BlkRequest, StopRequest>;

  std::shared_ptr<std::pmr::memory_resource> m_mr;

  rcp::dc::timestep_offset_type m_num_timesteps;

  using filler_type =
    BlockFiller<packet_type, block_type, FillImpl<packet_type, block_type>>;
  filler_type m_filler;

  using request_backlog_type = std::queue<request_type>;
  request_backlog_type m_requests;

  std::thread m_filler_thread;

  int m_request_fd;

  std::optional<timestamp_type> m_epoch;

  bool m_logged_blocks_while_stopped;

  std::optional<SweepParameters> m_sweep;

  using mutex_type = std::mutex;
  mutex_type mutable m_mtx;

  std::pmr::polymorphic_allocator<packet_type> m_packet_allocator;

  using Clock = std::chrono::system_clock;

  void
  run() {

    using namespace std::string_literals;

    auto tr = trace();

    auto& derived = static_cast<Derived&>(*this);

    auto& sweep = derived.m_sweep;

    static constexpr auto request_pidx = 0;
    static constexpr auto capture_pidx = 1;
    bool stop = false;
    std::array<pollfd, 2> pfds{};
    pfds[request_pidx] = pollfd{.fd = m_request_fd, .events = POLLIN};
    pfds[capture_pidx] = pollfd{.fd = derived.capture_fd(), .events = POLLIN};

    std::optional<timestamp_type> epoch;
    std::chrono::time_point<Clock> last_sweep;

    int poll_timeout = sweep ? (sweep->period.count() / 2) : -1;
    while (!stop) {
      {
        int const rc = poll(pfds.data(), pfds.size(), poll_timeout);
        if (rc < 0)
          log().fatal("Error in call to poll(): "s + std::strerror(errno));
      }
      if (!epoch) {
        std::scoped_lock lk(m_mtx);
        if (m_epoch) {
          epoch = m_epoch;
          if (sweep)
            last_sweep =
              std::chrono::time_point<Clock>(std::chrono::nanoseconds(*epoch))
              + sweep->delay;
        }
      }
      if ((pfds[capture_pidx].revents & POLLIN) != 0)
        handle_captures(pfds[capture_pidx].fd, epoch);
      if ((pfds[request_pidx].revents & POLLIN) != 0)
        stop = !handle_requests(pfds[request_pidx].fd);
      if (sweep && epoch)
        last_sweep = do_sweep(*sweep, last_sweep);
    }
  }

  void
  handle_captures(int fd, std::optional<timestamp_type> const& epoch) {
    eventfd_t ev{};
    [[maybe_unused]] auto rc = eventfd_read(fd, &ev);
    assert(rc == 0);
    auto& derived = static_cast<Derived&>(*this);
    auto captured = derived.capture();
    if (epoch)
      m_filler.process_packets(*epoch, captured);
    for (auto&& pkt : captured)
      m_packet_allocator.deallocate(pkt, 1);
  }

  auto
  handle_requests(int fd) -> bool {
    eventfd_t ev{};
    [[maybe_unused]] auto rc = eventfd_read(fd, &ev);
    assert(rc == 0);
    decltype(m_requests) requests;
    {
      std::scoped_lock lk(m_mtx);
      requests = std::move(m_requests);
    }
    bool result = true;
    while (requests.size() > 0) {
      std::visit(
        overloaded{
          [&](BlkRequest& req) {
            m_filler.fill(req.block, std::move(req.promise));
          },
          [&](StopRequest& /*req*/) { result = false; }},
        requests.front());
      requests.pop();
    }
    return result;
  }

  auto
  do_sweep(
    SweepParameters const& swpars, std::chrono::time_point<Clock> last_sweep)
    -> std::chrono::time_point<Clock> {
    auto now = Clock::now();
    if (now > last_sweep) {
      auto ms = std::chrono::floor<std::chrono::milliseconds>(now - last_sweep);
      if (ms >= swpars.period) {
        last_sweep += (ms / swpars.period) * swpars.period;
        m_filler.sweep(std::chrono::duration_cast<std::chrono::nanoseconds>(
                         (last_sweep - swpars.delay).time_since_epoch())
                         .count());
      }
    }
    return last_sweep;
  }

protected:
  auto
  mtx() -> mutex_type& {
    return m_mtx;
  }

  auto
  packet_allocator() -> std::pmr::polymorphic_allocator<packet_type>& {
    return m_packet_allocator;
  }

public:
  DataCapture() = delete;

  DataCapture(
    std::shared_ptr<std::pmr::memory_resource> mr,
    rcp::dc::timestep_offset_type num_timesteps,
    std::optional<SweepParameters> const& sweep = std::nullopt,
    unsigned max_packet_arrival_skew_factor = 1)
    : m_mr(std::move(mr))
    , m_num_timesteps(num_timesteps)
    , m_filler(filler_type(m_num_timesteps, max_packet_arrival_skew_factor))
    , m_request_fd(eventfd(0, 0))
    , m_logged_blocks_while_stopped(false)
    , m_sweep(sweep)
    , m_packet_allocator(m_mr.get()) {

    auto tr = trace();
  }

  DataCapture(const DataCapture&) = delete;

  DataCapture(DataCapture&&) noexcept = default;

  virtual ~DataCapture() {

    using namespace std::string_literals;

    auto tr = trace();
    stop();
    if (close(m_request_fd) != 0)
      log().warn(
        "Close of eventfd request file descriptor failed: "s
        + std::strerror(errno));
  }

  auto
  operator=(const DataCapture&) -> DataCapture& = delete;

  auto
  operator=(DataCapture&&) noexcept -> DataCapture& = default;

  void
  start() {
    auto tr = trace();
    std::scoped_lock lk(m_mtx);
    m_epoch.reset();
    if (!m_filler_thread.joinable())
      m_filler_thread = std::thread([this]() { this->run(); });
  }

  void
  stop() {
    auto tr = trace();
    std::unique_lock lk(m_mtx);
    if (m_filler_thread.joinable()) {
      StopRequest request;
      m_requests.push(std::move(request));
      [[maybe_unused]] auto rc = eventfd_write(m_request_fd, 1);
      assert(rc == 0);
      lk.unlock();
      m_filler_thread.join();
    } else {
      lk.unlock();
    }
  }

  auto
  epoch() const -> std::optional<timestamp_type> {
    auto tr = trace();
    std::scoped_lock lk(m_mtx);
    return m_epoch;
  }

  auto
  new_block(
    rcp::dc::channel_type channel_offset,
    rcp::dc::timestamp_type timestamp_offset,
    std::span<rcp::dc::fsample_type> const& samples,
    std::span<rcp::dc::fweight_type> const& weights) const -> block_type_sp {
    auto tr = trace();
    auto result = std::make_shared<block_type>(
      channel_offset, timestamp_offset, m_num_timesteps, samples, weights);
    assert(result->size() <= samples.size());
    return result;
  }

  /*! \brief fill a block
   *
   * \param blk pointer to a block
   *
   * \return future that is completed after block has been filled (to being
   * completely filled or a timeout), completed with a value equal to the input
   * parameter blk plus the count of filled samples, unless this DataCapture
   * instance is not active in which case a null pointer completes the future
   */
  auto
  fill(block_type_sp& blk) -> std::future<fill_result_t> {

    using namespace std::string_literals;

    auto tr = trace();
    assert([&]() -> bool {
      using extents_type = typename block_type::extents_type;
      using mapping_type = typename block_type::mapping_type;
      return blk->samples.size() >= mapping_type{
               extents_type{
                 m_num_timesteps}}.required_span_size();
    }());
    BlkRequest request;
    request.block = blk;
    auto result =
      std::get<std::promise<fill_result_t>>(request.promise).get_future();
    std::scoped_lock lk(m_mtx);
    if (m_filler_thread.joinable()) {
      log().info_if(
        [this]() {
          auto result = m_logged_blocks_while_stopped;
          m_logged_blocks_while_stopped = false;
          return result;
        },
        "Block passed to reactivated DataCapture instance: "s
          + "blocks will be processed");
      if (!m_epoch)
        m_epoch = blk->timestamp_offset;
      m_requests.push(std::move(request));
      [[maybe_unused]] auto rc = eventfd_write(m_request_fd, 1);
      assert(rc == 0);
    } else {
      log().info_if(
        [this]() {
          auto result = !m_logged_blocks_while_stopped;
          m_logged_blocks_while_stopped = true;
          return result;
        },
        "Block passed to inactive DataCapture instance: "s
          + "blocks will not be processed until instance becomes active");
      std::get<std::promise<fill_result_t>>(request.promise)
        .set_value({0, block_type_sp()});
    }
    return result;
  }

  /*! \brief fill a block
   *
   * \param blk pointer to a block
   * \param cb callback to be invoked when block has been filled
   */
  template <typename Fn>
    requires std::same_as<void, std::invoke_result_t<Fn, fill_result_t>>
  auto
  fill(block_type_sp& blk, Fn&& cb) -> void {

    auto tr = trace();
    assert([&]() -> bool {
      using extents_type = typename block_type::extents_type;
      using mapping_type = typename block_type::mapping_type;
      return blk->samples.size() >= mapping_type{
               extents_type{
                 m_num_timesteps}}.required_span_size();
    }());
    BlkRequest request;
    request.block = blk;
    request.promise = cb;
    std::scoped_lock lk(m_mtx);
    if (m_filler_thread.joinable()) {
      log().info_if(
        [this]() {
          auto result = m_logged_blocks_while_stopped;
          m_logged_blocks_while_stopped = false;
          return result;
        },
        "Block passed to reactivated DataCapture instance: "
        "blocks will be processed");
      if (!m_epoch)
        m_epoch = blk->timestamp_offset;
      m_requests.push(std::move(request));
      [[maybe_unused]] auto rc = eventfd_write(m_request_fd, 1);
      assert(rc == 0);
    } else {
      log().info_if(
        [this]() {
          auto result = !m_logged_blocks_while_stopped;
          m_logged_blocks_while_stopped = true;
          return result;
        },
        "Block passed to inactive DataCapture instance: "
        "blocks will not be processed until instance becomes active");
      std::invoke(cb, std::make_tuple(0, block_type_sp()));
    }
  }

  auto
  packet_memory_resource() -> std::shared_ptr<std::pmr::memory_resource> {
    return m_mr;
  }
};

} // end namespace rcp::dc

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
