# Copyright 2023 Martin Pokorny <mpokorny@caltech.edu>
# SPDX-License-Identifier: BSD-2-Clause-Patent

# test that RCPDC_MIN_LOG_LEVEL is in valid set
string(TOLOWER ${Rcpdc_MIN_LOG_LEVEL} Rcpdc_MIN_LOG_LEVEL_LOWER)
if(Rcpdc_MIN_LOG_LEVEL_LOWER STREQUAL "trace")
  set(Rcpdc_MIN_LOG_LEVEL_ENUMVAL Level::Trace)
elseif(Rcpdc_MIN_LOG_LEVEL_LOWER STREQUAL "debug")
  set(Rcpdc_MIN_LOG_LEVEL_ENUMVAL Level::Debug)
elseif(Rcpdc_MIN_LOG_LEVEL_LOWER STREQUAL "info")
  set(Rcpdc_MIN_LOG_LEVEL_ENUMVAL Level::Info)
elseif(Rcpdc_MIN_LOG_LEVEL_LOWER STREQUAL "warn")
  set(Rcpdc_MIN_LOG_LEVEL_ENUMVAL Level::Warn)
elseif(Rcpdc_MIN_LOG_LEVEL_LOWER STREQUAL "error")
  set(Rcpdc_MIN_LOG_LEVEL_ENUMVAL Level::Error)
elseif(Rcpdc_MIN_LOG_LEVEL_LOWER STREQUAL "fatal")
  set(Rcpdc_MIN_LOG_LEVEL_ENUMVAL Level::Fatal)
else()
  message(FATAL_ERROR
    "Invalid minimum log level; supported options are ${Rcpdc_LOG_LEVELS}")
endif()

# log variables in upper case are needed for configure_file()
set(RCPDC_TRACE_LOCATION ${Rcpdc_TRACE_LOCATION})
set(RCPDC_DEBUG_LOCATION ${Rcpdc_DEBUG_LOCATION})
set(RCPDC_INFO_LOCATION ${Rcpdc_INFO_LOCATION})
set(RCPDC_WARN_LOCATION ${Rcpdc_WARN_LOCATION})
set(RCPDC_ERROR_LOCATION ${Rcpdc_ERROR_LOCATION})
set(RCPDC_FATAL_LOCATION ${Rcpdc_FATAL_LOCATION})

set(RCPDC_ENABLE_OFI ${Rcpdc_ENABLE_OFI})

# write configuration values to header file
configure_file(config.hpp.in config.hpp)

# generate export header
include(GenerateExportHeader)
generate_export_header(rcpdc_rcpdc
  BASE_NAME rcpdc
  EXPORT_FILE_NAME export.hpp)

# add rcpdc library header files
if(Rcpdc_ENABLE_OFI)
  set(_data_capture_ofi DataCaptureOFI.hpp)
endif()
target_sources(rcpdc_rcpdc PUBLIC
  FILE_SET HEADERS
  FILES
    rcpdc.hpp
    BlockFiller.hpp
    DataCapture.hpp
    ${_data_capture_ofi}
    ${CMAKE_CURRENT_BINARY_DIR}/export.hpp
    ${CMAKE_CURRENT_BINARY_DIR}/config.hpp)
