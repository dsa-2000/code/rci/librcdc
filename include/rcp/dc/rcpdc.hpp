// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "rcp/dc/config.hpp"
#include "rcp/dc/export.hpp"

#include <bark/log.hpp>
#include <rcp/DataPacketConfiguration.hpp>
#include <rcp/rcp.hpp>

#include <cstdint>

namespace rcp::dc {

/*! tag type for bark logging in rcp::dc namespace */
struct Tag {};

} // end namespace rcp::dc

namespace bark {

/*! static context for bark logging in rcp::dc namespace */
template <>
struct StaticContext<rcp::dc::Tag> {
  static constexpr char const* nm = "rcpdc";
  static constexpr bark::Level min_log_level = RCPDC_MIN_LOG_LEVEL;
  static constexpr bool trace_location = RCPDC_TRACE_LOCATION;
  static constexpr bool debug_location = RCPDC_DEBUG_LOCATION;
  static constexpr bool info_location = RCPDC_INFO_LOCATION;
  static constexpr bool warn_location = RCPDC_WARN_LOCATION;
  static constexpr bool error_location = RCPDC_ERROR_LOCATION;
  static constexpr bool fatal_location = RCPDC_FATAL_LOCATION;
  static constexpr bool print_location = false;
  static constexpr auto
  source_identity() -> char const* {
    return nm;
  }
};

} // namespace bark

namespace rcp {

template <>
auto
get_log_sink<rcp::dc::Tag>() -> LogSink<rcp::dc::Tag>&;

} // namespace rcp

namespace rcp::dc {

// NOLINTBEGIN(cppcoreguidelines-avoid-non-const-global-variables)
extern rcp::LogSink<Tag> log_rcpdc;
// NOLINTEND(cppcoreguidelines-avoid-non-const-global-variables)

/*!  log instance for bark logging in rcp::dc namespace */
template <typename T = Tag>
inline auto
log() {
  return bark::Log<T>{get_log_sink<T>()};
}

/*! scope trace log instance for bark logging in rcp::dc namespace */
template <typename T = Tag>
inline auto
trace(std::source_location loc = bark::source_location::current()) {
  return bark::ScopeTraceLog<T>(get_log_sink<T>(), loc);
}

using receiver_type = DataPacketConfiguration::receiver_type;
using timestamp_type = DataPacketConfiguration::timestamp_type;
using timestep_offset_type = DataPacketConfiguration::timestep_offset_type;
using channel_type = DataPacketConfiguration::channel_type;
using ch_offset_type = DataPacketConfiguration::ch_offset_type;
using polarization_type = DataPacketConfiguration::polarization_type;
using sequence_type = DataPacketConfiguration::sequence_type;
using fsample_type = DataPacketConfiguration::fsample_type;
using fweight_type = DataPacketConfiguration::fweight_type;
using fvalue_type = DataPacketConfiguration::fvalue_type;

/*! apply a constant offset to a std::integer_sequence */
template <class T, T Offset, typename Seq>
struct offset_integer_sequence;

/*! apply a constant offset to each of a sequence of integers */
template <class T, T Offset, T... Ints>
struct offset_integer_sequence<T, Offset, std::integer_sequence<T, Ints...>> {
  using type = std::integer_sequence<T, (Ints + Offset)...>;
};

/*! mapping for a tiled layout of samples
 *
 * This mapping is defined for any extents with rank greater than 2. Tiles have
 * size 1 in the first dimension, and TS_TILE in the last dimension, and tiles
 * are ordered with the second dimension changing most rapidly. Within a tile,
 * elements follow a std::layout_right ordering.
 *
 * tile ordering: (ch0, ts0), (ch0, tsTS_TILE), ... (ch0, tsLAST), (ch1, ts0),
 * (ch1, tsTS_TILE), ... (ch1, tsLAST), ...
 */
template <timestep_offset_type TS_TILE>
struct TiledSamplesLayout {

  template <class Extents>
  struct mapping {

    static_assert(Extents::rank() > 2);

    // only the last dimension can have std::dynamic_extent
    template <typename Seq>
    struct no_dynamic;
    template <std::size_t... Dims>
    struct no_dynamic<std::integer_sequence<std::size_t, Dims...>> {
      static constexpr bool val =
        (... && (Extents::static_extent(Dims) != std::dynamic_extent));
    };
    static_assert(
      no_dynamic<typename std::make_index_sequence<Extents::rank() - 1>>::val);

    using extents_type = Extents;
    using rank_type = typename Extents::rank_type;
    using size_type = typename Extents::size_type;
    using layout_type = TiledSamplesLayout;

    mapping() noexcept = default;
    mapping(const mapping&) noexcept = default;
    mapping(mapping&&) noexcept = default;
    constexpr mapping(const extents_type& ext) noexcept : extents_(ext) {}
    auto
    operator=(const mapping&) noexcept -> mapping& = default;
    auto
    operator=(mapping&&) noexcept -> mapping& = default;
    ~mapping() = default;

    // tile layout
    template <typename Seq>
    struct tile_mapping;
    template <class T, T... Ints>
    struct tile_mapping<std::integer_sequence<T, Ints...>> {
      using ext_type = std::experimental::
        extents<size_type, Extents::static_extent(Ints)..., size_type{TS_TILE}>;
      using type = std::experimental::layout_right::mapping<ext_type>;
    };

    using tile_mapping_type =
      typename tile_mapping<typename offset_integer_sequence<
        rank_type,
        1,
        std::make_integer_sequence<rank_type, Extents::rank() - 2>>::type>::
        type;

    //------------------------------------------------------------
    // Helper members (not part of the layout concept)

    /*! number of tiles in timestep dimension */
    constexpr auto
    num_ts_tiles() const noexcept -> size_type {
      return (extents_.extent(Extents::rank() - 1) + TS_TILE - 1) / TS_TILE;
    }

    /*! number of elements in a tile */
    constexpr auto
    tile_size() const noexcept -> size_type {
      return tile_mapping_type{}.required_span_size();
    }

    /*! element offset of the tile into which an index is mapped */
    constexpr auto
    tile_offset(auto... Indexes) const noexcept -> size_type {

      static_assert(sizeof...(Indexes) == Extents::rank());
      return (std::get<0>(std::tuple(Indexes...)) * num_ts_tiles()
              + std::get<Extents::rank() - 1>(std::tuple(Indexes...)) / TS_TILE)
             * tile_size();
    }

    /*! element offset of an index relative to the tile into which it is
     *  mapped */
    constexpr auto
    offset_in_tile(size_type /*ch*/, auto... Indexes) const noexcept
      -> size_type {

      static_assert(sizeof...(Indexes) == Extents::rank() - 1);

      auto idxt = std::tuple(Indexes...);
      return tile_mapping_offset(
        select_elements(
          idxt,
          std::make_integer_sequence<size_type, sizeof...(Indexes) - 1>{}),
        std::get<Extents::rank() - 2>(idxt));
    }

    //------------------------------------------------------------
    // Required members

    constexpr auto
    extents() const -> extents_type const& {
      return extents_;
    }

    constexpr auto
    required_span_size() const noexcept -> size_type {
      return extents_.extent(0) * num_ts_tiles() * tile_size();
    }

    constexpr auto
    operator()(auto... Indexes) const noexcept -> size_type {
      return tile_offset(Indexes...) + offset_in_tile(Indexes...);
    }

    // Mapping is always unique
    static constexpr auto
    is_always_unique() noexcept -> bool {
      return true;
    }
    // Only exhaustive if extents_.extent(0) % TS_TILE == 0, so not always
    static constexpr auto
    is_always_exhaustive() noexcept -> bool {
      return false;
    }
    // There is not always a regular stride between elements in a given
    // dimension
    static constexpr auto
    is_always_strided() noexcept -> bool {
      return false;
    }

    static constexpr auto
    is_unique() noexcept -> bool {
      return true;
    }

    [[nodiscard]] constexpr auto
    is_exhaustive() const noexcept -> bool {
      // Only exhaustive if extents fit exactly into tile sizes...
      return extents_.extent(Extents::rank() - 1) % TS_TILE == 0;
    }
    // There are some circumstances where this is strided, but we're not
    // concerned about that optimization, so we're allowed to just return false
    // here
    [[nodiscard]] constexpr auto
    is_strided() const noexcept -> bool {
      return false;
    }

  private:
    /*! make tuple of selected elements of a tuple
     */
    template <size_type... Indexes>
    static constexpr auto
    select_elements(
      auto tpl, std::integer_sequence<size_type, Indexes...> const& /*seq*/) {

      return std::make_tuple(std::get<Indexes>(tpl)...);
    }

    /*! mapping offset within a tile
     *
     * @param idxt0 tile index tuple of all but timestep dimension
     * @param ts timestep index (raw value, before computation of remainder)
     */
    static constexpr auto
    tile_mapping_offset(auto idxt0, size_type ts) {
      return tile_mapping_offset(
        idxt0,
        ts,
        std::make_integer_sequence<
          size_type,
          std::tuple_size_v<decltype(idxt0)>>{});
    }

    /*! mapping offset within a tile
     *
     * @param idxt0 tile index tuple of all but timestep dimension
     * @param ts timestep index (raw value, before computation of remainder)
     *
     * This is used by the method without the integer_sequence argument.
     */
    template <size_type... Indexes>
    static constexpr auto
    tile_mapping_offset(
      auto idxt0,
      size_type ts,
      std::integer_sequence<size_type, Indexes...> const& /*seq*/) {

      return tile_mapping_type{}(std::get<Indexes>(idxt0)..., ts % TS_TILE);
    }

    Extents extents_;
  };
};

template <
  unsigned SampleInterval,
  ch_offset_type NumChannels,
  polarization_type NumPolarizations,
  timestep_offset_type NumTimesteps,
  timestep_offset_type TimestepsTileSize>
struct FPacket {
  static constexpr auto sample_interval_ns = SampleInterval;
  static constexpr auto num_channels = NumChannels;
  static constexpr auto num_polarizations = NumPolarizations;
  static constexpr auto num_timesteps = NumTimesteps;
  static constexpr auto timesteps_tile_size = TimestepsTileSize;
  static constexpr std::size_t size = std::size_t(num_channels)
                                      * std::size_t(num_polarizations)
                                      * std::size_t(num_timesteps);
  using extents_type = std::experimental::extents<
    std::common_type_t<ch_offset_type, polarization_type, timestep_offset_type>,
    num_channels,
    num_polarizations,
    num_timesteps>;
  using mapping_type = std::conditional_t<
    (timesteps_tile_size > 0),
    typename TiledSamplesLayout<TimestepsTileSize>::template mapping<
      extents_type>,
    typename std::experimental::layout_right::mapping<extents_type>>;

  // NOLINTBEGIN(misc-non-private-member-variables-in-classes)
  receiver_type receiver{};
  timestamp_type timestamp_offset{};
  channel_type channel_offset{};
  std::array<fsample_type, mapping_type{}.required_span_size()> fsamples{};
  // NOLINTEND(misc-non-private-member-variables-in-classes)

  static constexpr auto
  mapping() {
    return mapping_type{};
  }
  static constexpr auto
  channels() {
    return std::views::iota(ch_offset_type{0}, num_channels);
  }
  static constexpr auto
  polarizations() {
    return std::views::iota(polarization_type{0}, num_polarizations);
  }
  static constexpr auto
  timesteps() {
    return std::views::iota(timestep_offset_type{0}, num_timesteps);
  }
  static constexpr auto
  duration() {
    return num_timesteps * sample_interval_ns;
  }
  constexpr auto
  fsamples_mdspan() {
    return std::experimental::mdspan(fsamples.data(), mapping());
  }
};

template <typename T>
concept IsFPacket = requires(T val) {
  val.sample_interval_ns;
  val.num_channels;
  val.num_polarizations;
  val.num_timesteps;
  val.timesteps_tile_size;
  val.size;
  typename T::extents_type;
  typename T::mapping_type;
  { val.receiver } -> std::convertible_to<receiver_type>;
  { val.timestamp_offset } -> std::convertible_to<timestamp_type>;
  { val.channel_offset } -> std::convertible_to<channel_type>;
  { val.fsamples[0] } -> std::convertible_to<fsample_type>;
};

template <
  unsigned SampleInterval,
  receiver_type NumReceivers,
  ch_offset_type NumChannels,
  polarization_type NumPolarizations,
  timestep_offset_type TimestepsTileSize>
struct FBlock {
  static constexpr auto sample_interval_ns = SampleInterval;
  static constexpr auto num_channels = NumChannels;
  static constexpr auto num_receivers = NumReceivers;
  static constexpr auto num_polarizations = NumPolarizations;
  static constexpr auto timesteps_tile_size = TimestepsTileSize;
  using extents_type = std::experimental::extents<
    std::common_type_t<
      ch_offset_type,
      receiver_type,
      polarization_type,
      timestep_offset_type>,
    num_channels,
    num_receivers,
    num_polarizations,
    std::dynamic_extent>;
  using mapping_type = std::conditional_t<
    (timesteps_tile_size > 0),
    typename TiledSamplesLayout<TimestepsTileSize>::template mapping<
      extents_type>,
    typename std::experimental::layout_right::mapping<extents_type>>;

  // NOLINTBEGIN(misc-non-private-member-variables-in-classes)
  channel_type channel_offset{};
  timestamp_type timestamp_offset{};
  timestep_offset_type num_timesteps{};
  std::span<fsample_type> samples{};
  std::span<fweight_type> weights{};
  mapping_type mapping;
  // NOLINTEND(misc-non-private-member-variables-in-classes)

  // NOLINTBEGIN(bugprone-easily-swappable-parameters)
  FBlock(
    channel_type channel_offset_,
    timestamp_type timestamp_offset_,
    timestep_offset_type num_timesteps_,
    std::span<fsample_type> const& samples_,
    std::span<fweight_type> const& weights_)
    : channel_offset(channel_offset_)
    , timestamp_offset(timestamp_offset_)
    , num_timesteps(num_timesteps_)
    , samples(samples_)
    , weights(weights_)
    , mapping(extents_type{num_timesteps}) {}
  // NOLINTEND(bugprone-easily-swappable-parameters)

  static auto
  size(timestep_offset_type num_ts) -> std::size_t {
    return std::size_t(num_receivers) * std::size_t(num_channels)
           * std::size_t(num_polarizations) * std::size_t(num_ts);
  }
  [[nodiscard]] auto
  size() const -> std::size_t {
    return std::size_t(num_receivers) * std::size_t(num_channels)
           * std::size_t(num_polarizations) * std::size_t(num_timesteps);
  }
  static constexpr auto
  channels() {
    return std::views::iota(ch_offset_type{0}, num_channels);
  }
  static constexpr auto
  receivers() {
    return std::views::iota(receiver_type{0}, num_receivers);
  }
  static constexpr auto
  polarizations() {
    return std::views::iota(polarization_type{0}, num_polarizations);
  }
  auto
  timesteps() const {
    return std::views::iota(timestep_offset_type{0}, num_timesteps);
  }
  auto
  duration() const {
    return num_timesteps * timestamp_type(sample_interval_ns);
  }
  auto
  samples_mdspan() {
    return std::experimental::mdspan(samples.data(), mapping);
  }
  auto
  weights_mdspan() {
    return std::experimental::mdspan(weights.data(), mapping);
  }
};

template <typename T>
concept IsFBlock = requires(T val) {
  val.sample_interval_ns;
  val.num_channels;
  val.num_receivers;
  val.num_polarizations;
  val.num_timesteps;
  val.timesteps_tile_size;
  val.size();
  typename T::extents_type;
  typename T::mapping_type;
  { val.timestamp_offset } -> std::convertible_to<timestamp_type>;
  { val.channel_offset } -> std::convertible_to<channel_type>;
  { val.num_timesteps } -> std::convertible_to<timestep_offset_type>;
  { val.samples[0] } -> std::convertible_to<fsample_type>;
  { val.weights[0] } -> std::convertible_to<fweight_type>;
};

} // namespace rcp::dc

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
