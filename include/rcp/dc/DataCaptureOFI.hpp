// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#pragma once

#include "DataCapture.hpp"
#include <algorithm>
#include <cstdint>
#include <cstdlib>
#include <netinet/in.h>
#include <oficpp/oficpp.hpp>

#include <chrono>
#include <condition_variable>
#include <cstring>
#include <memory>
#include <mutex>
#include <optional>
#include <rdma/fi_eq.h>
#include <set>
#include <stack>
#include <thread>
#include <type_traits>
#include <variant>
#include <vector>

#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>

#ifndef RCPDC_ENABLE_OFI
#error "rcpdc configured without libfabric"
#endif

namespace rcp::dc {

using namespace std::chrono_literals;
using namespace std::string_literals;

template <unsigned N, IsFPacket P>
struct FPacketContext {
  std::array<fi_context, N> hdr;
  P* packet;
};

class RCPDC_EXPORT prefixed_memory_resource : public std::pmr::memory_resource {

  std::size_t m_offset;

  std::pmr::memory_resource* m_base_mr;

public:
  prefixed_memory_resource(
    std::size_t offset, std::pmr::memory_resource* base_mr)
    : m_offset{offset}, m_base_mr{base_mr} {

    auto tr = trace();
  }

  prefixed_memory_resource(const prefixed_memory_resource&) = default;

  prefixed_memory_resource(prefixed_memory_resource&&) = default;

  ~prefixed_memory_resource() override = default;

  auto
  operator=(const prefixed_memory_resource&)
    -> prefixed_memory_resource& = default;

  auto
  operator=(prefixed_memory_resource&&) -> prefixed_memory_resource& = default;

  [[nodiscard]] auto
  offset() const -> std::size_t {
    return m_offset;
  }

  auto
  do_allocate(std::size_t bytes, std::size_t alignment) -> void* override {
    auto tr = trace();
    assert(m_offset % alignment == 0);
    return
      // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
      reinterpret_cast<char*>(m_base_mr->allocate(bytes + m_offset, alignment))
      + m_offset; // NOLINT(cppcoreguidelines-pro-bounds-pointer-arithmetic)
  }

  void
  do_deallocate(void* ptr, std::size_t bytes, std::size_t alignment) override {
    auto tr = trace();
    m_base_mr->deallocate(
      // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast,cppcoreguidelines-pro-bounds-pointer-arithmetic)
      reinterpret_cast<char*>(ptr) - m_offset,
      bytes + m_offset,
      alignment);
  }

  [[nodiscard]] auto
  do_is_equal(std::pmr::memory_resource const& other) const noexcept
    -> bool override {
    auto tr = trace();
    try {
      auto const& other_pmr =
        dynamic_cast<prefixed_memory_resource const&>(other);
      return m_base_mr->is_equal(*other_pmr.m_base_mr)
             && m_offset == other_pmr.m_offset;
    } catch (std::bad_cast const&) {
      return false;
    }
  }
};

class RCPDC_EXPORT prefixed_single_block_pool
  : public prefixed_memory_resource {

  void* m_address;

  std::size_t m_size;

public:
  static constexpr auto has_single_block_memory_resource = true;

  template <HasSingleBlockMemoryResource M>
  prefixed_single_block_pool(std::size_t offset, M* sbmr)
    : prefixed_memory_resource(offset, sbmr)
    , m_address(sbmr->address())
    , m_size(sbmr->size()) {

    auto tr = trace();
  }

  auto
  address() -> void* {
    auto tr = trace();
    return m_address;
  }

  [[nodiscard]] auto
  size() const -> std::size_t {
    auto tr = trace();
    return m_size;
  }
};

/*! aggregate of addr_format_t and name of an endpoint */
struct RCPDC_EXPORT EndpointName {
  using addr_format_t = decltype(fi_info::addr_format);
  // NOLINTBEGIN(misc-non-private-member-variables-in-classes)
  addr_format_t format{};
  std::uint32_t namelen{};
  std::array<char, FI_NAME_MAX> name{};
  // NOLINTEND(misc-non-private-member-variables-in-classes)

  EndpointName() = default;
  EndpointName(addr_format_t const& fmt, std::vector<char> const& nm)
    : format(fmt), namelen(nm.size()) {
    std::ranges::copy(nm, name.begin());
  }
  EndpointName(EndpointName const&) = default;
  EndpointName(EndpointName&&) = default;
  ~EndpointName() = default;
  auto
  operator=(EndpointName const&) -> EndpointName& = default;
  auto
  operator=(EndpointName&&) -> EndpointName& = default;
  auto
  operator==(EndpointName const& rhs) const -> bool {
    return std::memcmp(&format, &rhs.format, sizeof(format)) == 0
           && std::memcmp(name.data(), rhs.name.data(), namelen) == 0;
  }
};

/*! data capture broadcast message */
template <unsigned MaxNumPacketOffsets>
struct Message {

  /*! element value in channel_offsets that indicates an invalid entry */
  static constexpr channel_type invalid_channel_offset =
    std::numeric_limits<channel_type>::max();

  /*! format and name of capture endpoint */
  EndpointName endpoint_name;
  /*! (channel) streams captured by this endpoint */
  std::array<channel_type, MaxNumPacketOffsets> channel_offsets;
};

/*! broadcast of packet channel offsets associated with a DataCapture
 *  instance
 */
template <unsigned MaxNumPacketOffsets>
class Bcast {

  static_assert(MaxNumPacketOffsets > 0);

  struct SetPeriod {
    std::chrono::seconds period;
  };
  struct Stop {};
  using request_type = std::variant<SetPeriod, Stop>;

  std::queue<request_type> m_requests;

  std::mutex m_mtx;

  std::condition_variable m_cv;

  std::thread m_thread;

  EndpointName m_endpoint_name;

  std::set<channel_type> m_packet_channel_offsets;

  std::chrono::seconds m_init_period;

  in_port_t m_mc_port{};

  in_addr m_mc_addr{};

  void
  run(std::chrono::seconds mc_period) {

    auto tr = trace();

    std::chrono::seconds timeout_period;
    // if we're multicasting destination addresses, then open a socket that can
    // send to the multicast group
    int sock = -1;
    sockaddr_in mc_addr;
    if (mc_period != std::chrono::seconds{0}) {
      sock = socket(AF_INET, SOCK_DGRAM, 0);
      assert(sock != -1);
      mc_addr = {
        .sin_family = AF_INET, .sin_port = m_mc_port, .sin_addr = m_mc_addr};
      {
        int trueval{1};
        auto rc = setsockopt(
          sock, IPPROTO_IP, IP_MULTICAST_LOOP, &trueval, sizeof(trueval));
        assert(rc == 0);
        if (rc != 0)
          std::abort();
      }
      timeout_period = mc_period;
    } else {
      // assign a period, for timeout purposes
      timeout_period = std::chrono::seconds{1};
    }
    // construct the message that goes out to the multicast group
    Message<MaxNumPacketOffsets> bcast_msg;
    bcast_msg.endpoint_name = m_endpoint_name;
    auto bcast_ch = bcast_msg.channel_offsets.begin();
    for (auto&& ch : m_packet_channel_offsets)
      *bcast_ch++ = htons(ch);
    auto invalid = htons(Message<MaxNumPacketOffsets>::invalid_channel_offset);
    while (bcast_ch != bcast_msg.channel_offsets.end())
      *bcast_ch++ = invalid;

    bool stop = false;
    while (!stop) {
      // send multicast only if socket was created
      if (sock != -1) {
        [[maybe_unused]] auto rc = sendto(
          sock,
          &bcast_msg,
          sizeof(bcast_msg),
          0,
          // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
          reinterpret_cast<sockaddr const*>(&mc_addr),
          sizeof(mc_addr));
        assert(rc == ssize_t(sizeof(bcast_msg)));
      }
      {
        std::unique_lock lk(m_mtx);
        bool timeout = false;
        while (!timeout && m_requests.size() == 0)
          timeout = !m_cv.wait_for(
            lk, timeout_period, [this]() { return m_requests.size() > 0; });
        while (m_requests.size() > 0) {
          std::visit(
            overloaded{
              [&](SetPeriod& req) {
                if (mc_period != std::chrono::seconds{0}) {
                  mc_period = req.period;
                  timeout_period = mc_period;
                } else if (req.period > std::chrono::seconds{0}) {
                  log().debug("Attempt to set period of DataCapture "
                              "without transmit multicast: ignored");
                }
              },
              [&](Stop& /*req*/) { stop = true; }},
            m_requests.front());
          m_requests.pop();
        }
      }
    }
    if (sock != -1) {
      auto rc = close(sock);
      if (rc != 0)
        log().fatal(
          "Failed to close multicast socket: "s + std::strerror(errno));
    }
  }

public:
  Bcast(
    decltype(fi_info::addr_format) const& endpoint_addr_format,
    std::vector<char> const& endpoint_name,
    std::string_view const& multicast_addr,
    std::set<channel_type> const& packet_channel_offsets,
    std::optional<std::chrono::seconds> const& init_period)
    : m_endpoint_name(endpoint_addr_format, endpoint_name)
    , m_packet_channel_offsets(packet_channel_offsets)
    , m_init_period(init_period.value_or(std::chrono::seconds{0})) {

    auto tr = trace();

    if (packet_channel_offsets.size() > MaxNumPacketOffsets)
      throw std::runtime_error("FIXME");

    if (!multicast_addr.empty()) {
      auto colon = multicast_addr.find(':');
      if (colon == std::string_view::npos)
        throw std::runtime_error(
          "Failed to identify port number in multicast address");
      auto node = std::string(multicast_addr.substr(0, colon));
      auto service = std::string(
        multicast_addr.substr(colon + 1, std::string_view::npos - (colon + 1)));
      m_mc_port = htons(std::uint16_t(std::stoul(service)));
      [[maybe_unused]] auto rc = inet_pton(AF_INET, node.c_str(), &m_mc_addr);
      assert(rc == 1);
    }
  }

  Bcast(Bcast const&) = delete;

  Bcast(Bcast&&) noexcept = default;

  virtual ~Bcast() {
    auto tr = trace();
    stop();
  }

  auto
  operator=(Bcast const&) -> Bcast& = delete;

  auto
  operator=(Bcast&&) noexcept -> Bcast& = default;

  void
  start() {
    auto tr = trace();
    std::scoped_lock lk(m_mtx);
    if (!m_thread.joinable())
      m_thread = std::thread([this]() { this->run(m_init_period); });
  }

  void
  stop() {
    auto tr = trace();
    std::unique_lock lk(m_mtx);
    if (m_thread.joinable()) {
      m_requests.push(Stop{});
      m_cv.notify_one();
      lk.unlock();
      m_thread.join();
    } else {
      lk.unlock();
    }
  }

  [[nodiscard]] auto
  get_period() const -> std::optional<std::chrono::seconds> {

    auto tr = trace();
    if (m_init_period > std::chrono::seconds{0})
      return m_init_period;
    return std::nullopt;
  }

  auto
  set_period(std::optional<std::chrono::seconds> const& period)
    -> std::optional<std::chrono::seconds> {

    auto tr = trace();
    std::unique_lock lk(m_mtx);
    m_init_period = period.value_or(std::chrono::seconds{0});
    if (m_thread.joinable()) {
      m_requests.push(SetPeriod{m_init_period});
      m_cv.notify_one();
    }
    return period;
  }
};

template <IsFPacket P>
struct DataCaptureOFIUtil {

  static int const fi_version = FI_VERSION(1, 15);

  static std::uint64_t const mr_key = 1234;

  static auto
  find_fabric_interfaces(std::string const& provider)
    -> std::vector<oficpp::FiInfo> {

    auto tr = trace();
    oficpp::FiInfo hints;
    hints->caps = FI_MSG;
    hints->mode = ~0;
    hints->domain_attr->mode = ~0;
    hints->domain_attr->mr_mode = ~(FI_MR_BASIC | FI_MR_SCALABLE);
    hints->fabric_attr->prov_name = strdup(provider.c_str());
    hints->ep_attr->type = FI_EP_DGRAM;
    hints->ep_attr->max_msg_size = sizeof(P);
    hints->caps |= FI_RECV;
    return hints.getinfo(fi_version);
  }

  static inline auto
  requires_rx_message_prefix(oficpp::FiInfo const& info) -> bool {
    return (info->rx_attr->mode & FI_MSG_PREFIX) != 0;
  }

  static inline auto
  rx_message_prefix_size(oficpp::FiInfo const& info) {
    return requires_rx_message_prefix(info) ? info->ep_attr->msg_prefix_size
                                            : 0;
  }

  // TODO: how do we account for the prefix in packet deallocation by memory
  // resource??
  static auto
  rx_message_size(oficpp::FiInfo const& info) -> std::size_t {

    auto tr = trace();
    return rx_message_prefix_size(info) + sizeof(P);
  }
};

template <
  IsFPacket P,
  IsFBlock B,
  unsigned MaxNumPacketOffsets,
  template <typename, typename>
  typename FillImpl>
class DataCaptureOFIBase
  : public DataCapture<
      P,
      B,
      FillImpl,
      DataCaptureOFIBase<P, B, MaxNumPacketOffsets, FillImpl>>
  , public DataCaptureOFIUtil<P> {
public:
  using dc_type = DataCapture<
    P,
    B,
    FillImpl,
    DataCaptureOFIBase<P, B, MaxNumPacketOffsets, FillImpl>>;
  using packet_type = typename dc_type::packet_type;
  using packet_type_p = typename dc_type::packet_type_p;
  using block_type = typename dc_type::block_type;

  using typename dc_type::DataCapture;

  virtual void
  start() = 0;

  virtual void
  stop() = 0;

  using pkt_mdspan_type =
    decltype(std::declval<packet_type>().fsamples_mdspan());
  using bsmp_mdspan_type =
    decltype(std::declval<block_type>().samples_mdspan());
  using bwgt_mdspan_type =
    decltype(std::declval<block_type>().weights_mdspan());
  using size_type = typename pkt_mdspan_type::size_type;

  static void
  copy_packet_to_block(
    std::size_t /*num_ts*/,
    pkt_mdspan_type const& /*packet*/,
    size_type /*pkt_ts0*/,
    bsmp_mdspan_type const& /*samples*/,
    bwgt_mdspan_type const& /*weights*/,
    size_type /*receiver*/,
    size_type /*blk_ts0*/) {

    auto tr = trace();
  }

  virtual auto
  capture_fd() -> int = 0;

  virtual auto
  capture() -> std::vector<packet_type_p> = 0;

  [[nodiscard]] virtual auto
  get_bcast_period() const -> std::optional<std::chrono::seconds> = 0;

  virtual auto
  set_bcast_period(std::optional<std::chrono::seconds> const& period)
    -> std::optional<std::chrono::seconds> = 0;
};

/*! DataCaptureOFI class
 *
 * \tparam P fully specialized FPacket class
 * \tparam B fully specialized FBlock class
 */
template <
  IsFPacket P,
  IsFBlock B,
  unsigned MaxNumPacketOffsets,
  template <typename, typename>
  typename FillImpl,
  unsigned ContextSz>
class DataCaptureOFI
  : public DataCaptureOFIBase<P, B, MaxNumPacketOffsets, FillImpl> {

public:
  using dc_ofi_type = DataCaptureOFIBase<P, B, MaxNumPacketOffsets, FillImpl>;
  using dc_type = typename dc_ofi_type::dc_type;
  using packet_type = typename dc_type::packet_type;
  using packet_type_p = typename dc_type::packet_type_p;
  using block_type = typename dc_type::block_type;

private:
  struct Endpoint {

    // NOLINTBEGIN(misc-non-private-member-variables-in-classes)
    std::shared_ptr<oficpp::FidEndpoint<FPacketContext<ContextSz, packet_type>>>
      ep;

    std::shared_ptr<oficpp::FidMr<>> mr;

    std::shared_ptr<oficpp::FidCq<>> cq;

    std::shared_ptr<oficpp::FidEq<>> eq;

    /*! \brief set of context pointers for which a FI_JOIN_COMPLETE event is
     * expected but has not been received
     *
     * Element value type is channel_type, but only the pointers are used, and
     * need not ever be dereferenced.
     */
    std::set<std::shared_ptr<channel_type>> mc_pending_context{};
    // NOLINTEND(misc-non-private-member-variables-in-classes)

    Endpoint(
      void* memory_resource_buffer,
      // NOLINTBEGIN(bugprone-easily-swappable-parameters)
      std::size_t memory_resource_size,
      std::size_t cq_size,
      // NOLINTEND(bugprone-easily-swappable-parameters)
      oficpp::FiInfo info,
      std::string_view const& multicast_addr,
      std::set<channel_type> const& packet_channel_offsets) {

      auto tr = trace();
      auto fabric = oficpp::FidFabric<>::create(info);
      auto domain = oficpp::FidDomain<>::create(fabric, info);
      ep = oficpp::FidEndpoint<FPacketContext<ContextSz, packet_type>>::create(
        domain, info);
      fi_eq_attr eq_attr{
        .size = 0,
        .flags = 0,
        .wait_obj = FI_WAIT_NONE,
        .signaling_vector = 0,
        .wait_set = nullptr};
      eq = oficpp::FidEq<>::create(fabric, eq_attr);
      ep->bind(eq);
      fi_cq_attr cq_attr{
        .size = cq_size,
        .flags = 0,
        .format = FI_CQ_FORMAT_CONTEXT,
        .wait_obj = FI_WAIT_FD,
        .signaling_vector = 0,
        .wait_cond = FI_CQ_COND_NONE,
        .wait_set = nullptr};
      cq = oficpp::FidCq<>::create(domain, cq_attr);
      ep->bind(cq, FI_RECV);
      if ((info->domain_attr->mr_mode & FI_MR_LOCAL) != 0) {
        mr = oficpp::FidMr<>::create(
          domain,
          memory_resource_buffer,
          memory_resource_size,
          FI_RECV,
          0,
          dc_ofi_type::mr_key,
          0);
        if ((info->domain_attr->mr_mode & FI_MR_ENDPOINT) != 0) {
          mr->bind(ep);
          mr->enable();
        }
      }
      fi_av_attr av_attr{
        .type = info->domain_attr->av_type,
        .rx_ctx_bits = 0,
        .count = 1,
        .ep_per_node = 0, // TODO: optimize?
        .name = nullptr,
        .map_addr = nullptr,
        .flags = 0};
      ep->bind(oficpp::FidAv<>::create(domain, av_attr));

      // join sender multicast groups
      if (!multicast_addr.empty()) {
        auto colon = multicast_addr.find(':');
        if (colon == std::string_view::npos)
          throw std::runtime_error(
            "Failed to identify port number in multicast address");
        auto node = std::string(multicast_addr.substr(0, colon));
        auto service = std::string(multicast_addr.substr(
          colon + 1, std::string_view::npos - (colon + 1)));
        auto port_offset = htons(std::uint16_t(std::stoul(service)));
        auto mc_addr = sockaddr_in{.sin_family = AF_INET};
        [[maybe_unused]] auto rc =
          inet_pton(AF_INET, node.c_str(), &mc_addr.sin_addr);
        assert(rc == 1);
        for (auto const& ch_offset : packet_channel_offsets) {
          mc_addr.sin_port = htons(multicast_port(ch_offset, port_offset));
          auto join_context = std::make_shared<channel_type>(ch_offset);
          mc_pending_context.insert(join_context);
          ep->join(&mc_addr, 0, join_context);
        }
      }
      ep->enable();
    }

    /*! \brief map packet channel offset to sender multicast group port
     * number
     */
    static auto
    multicast_port(channel_type ch_offset, std::uint16_t port_offset)
      -> std::uint16_t {
      return std::uint16_t(ch_offset / P::num_channels) + port_offset;
    }

    /*! \brief record FI_JOIN_COMPLETE event by removing an entry with a given
     *  context pointer from ch_offset_mc_pending
     *
     * After return, ch_offset_mc_pending will not contain an element with the
     * pointer value context.
     *
     * \return true, iff context was found in pointer values of
     * ch_offset_mc_pending elements
     */
    auto
    record_mc_join(void const* context) -> bool {
      auto match =
        std::ranges::find_if(mc_pending_context, [=](auto const& chp) {
          return chp.get() == context;
        });
      if (match == mc_pending_context.end())
        return false;
      mc_pending_context.erase(match);
      return true;
    }
  };

  Endpoint m_endpoint;

  Bcast<MaxNumPacketOffsets> m_bcast;

  std::size_t m_packet_offset;

  std::stack<FPacketContext<ContextSz, packet_type>*> m_packet_contexts;

  std::vector<FPacketContext<ContextSz, packet_type>*> m_rx_completions;

public:
  /*! \brief constructor
   *
   * multicast is used for one of the following purposes:
   * - broadcasting (sending) receive interface info
   * - joining receive multicast group
   *
   * which of the above is operative depends on value of bcast_period:
   * - not empty, broadcast receive interface info
   * - empty, join receive multicast group
   */
  template <HasSingleBlockMemoryResource M>
  DataCaptureOFI(
    std::shared_ptr<M> const& sbmr,
    std::size_t cq_size,
    oficpp::FiInfo const& info,
    std::string_view const& multicast_addr,
    std::set<channel_type> const& packet_channel_offsets,
    timestep_offset_type num_ts,
    std::optional<SweepParameters> const& sweep = std::nullopt,
    unsigned max_packet_arrival_skew_factor = 1,
    std::optional<std::chrono::seconds> const& bcast_period = std::nullopt)
    : dc_ofi_type(
        (dc_ofi_type::requires_rx_message_prefix(info)
           ? std::static_pointer_cast<std::pmr::memory_resource>(
               std::make_shared<prefixed_single_block_pool>(
                 dc_ofi_type::rx_message_prefix_size(info), sbmr.get()))
           : std::static_pointer_cast<std::pmr::memory_resource>(sbmr)),
        num_ts,
        sweep,
        max_packet_arrival_skew_factor)
    , m_endpoint(
        sbmr->address(),
        sbmr->size(),
        cq_size,
        info,
        (!bcast_period ? multicast_addr : ""),
        (!bcast_period ? packet_channel_offsets : std::set<channel_type>{}))
    , m_bcast(
        m_endpoint.ep->addr_format(),
        m_endpoint.ep->getname(),
        (bcast_period ? multicast_addr : ""),
        (bcast_period ? packet_channel_offsets : std::set<channel_type>{}),
        bcast_period)
    , m_packet_offset(dc_ofi_type::rx_message_prefix_size(info))
    , m_rx_completions(cq_size) {

    auto tr = trace();
  }

  DataCaptureOFI(const DataCaptureOFI&) = delete;

  DataCaptureOFI(DataCaptureOFI&&) noexcept = default;

  virtual ~DataCaptureOFI() {

    auto tr = trace();
    while (!m_packet_contexts.empty()) {
      delete m_packet_contexts.top();
      m_packet_contexts.pop();
    }
  }

  auto
  operator=(const DataCaptureOFI&) -> DataCaptureOFI& = delete;

  auto
  operator=(DataCaptureOFI&&) noexcept -> DataCaptureOFI& = default;

  void
  start() override {
    auto tr = trace();
    post_recv(m_rx_completions.size());
    static_cast<dc_type*>(this)->start();
    m_bcast.start();
  }

  void
  stop() override {
    auto tr = trace();
    m_bcast.stop();
    static_cast<dc_type*>(this)->stop();
    // FIXME: pending receive requests?
  }

  auto
  capture_fd() -> int override {

    auto tr = trace();
    int result = 0;
    auto rc = fi_control(*m_endpoint.cq, FI_GETWAIT, &result);
    if (rc != 0)
      oficpp::abort_fi(rc);
    return result;
  }

  auto
  capture() -> std::vector<packet_type_p> override {

    auto tr = trace();

    // read from event queue
    uint32_t event;
    auto evread = m_endpoint.eq->peek(event);
    if (evread > 0) {
      switch (event) {
      case FI_JOIN_COMPLETE:
      case FI_AV_COMPLETE:
      case FI_MR_COMPLETE: {
        fi_eq_entry eq_entry{};
        m_endpoint.eq->read(event, eq_entry);
        if (event == FI_JOIN_COMPLETE) {
          if (!m_endpoint.record_mc_join(eq_entry.context))
            log().warn("Received unexpected (or duplicate) FI_JOIN_COMPLETE");
        }
        break;
      }
      default:
        std::abort();
        break;
      }
    } else if (evread == -FI_EAVAIL) {
      fi_eq_err_entry eq_err_entry{};
      eq_err_entry.err_data_size = 0;
      [[maybe_unused]] auto rc = m_endpoint.eq->readerr(eq_err_entry);
      assert(rc > 0);
      std::array<char, 256> buf;
      log().info(
        "Error on event queue: "s
        + fi_eq_strerror(
          *m_endpoint.eq,
          eq_err_entry.prov_errno,
          eq_err_entry.err_data,
          buf.data(),
          buf.size()));
    }

    // read from completion queue
    ssize_t nread = 0;
    bool retry = false;
    do {
      nread = m_endpoint.cq->read(std::span(m_rx_completions));
      if (nread == -FI_EAVAIL) {
        fi_cq_err_entry cq_err_entry{};
        [[maybe_unused]] auto rc = m_endpoint.cq->readerr(cq_err_entry);
        assert(rc == 1);
        log().info(
          "Receive error on completion queue: "s
          + fi_strerror(cq_err_entry.err));
      } else if (nread == -FI_EAGAIN) {
        nread = 0;
      }
      retry = (nread == -FI_EAVAIL) || (nread == -FI_EINTR);
    } while (retry);
    std::vector<packet_type_p> result;
    if (nread > 0) {
      result.reserve(nread);
      for (auto&& comp : std::views::iota(std::size_t{0}, std::size_t(nread))) {
        result.push_back(m_rx_completions[comp]->packet);
        m_packet_contexts.push(m_rx_completions[comp]);
      }
      post_recv(nread);
    }
    return result;
  }

  [[nodiscard]] auto
  get_bcast_period() const -> std::optional<std::chrono::seconds> override {
    auto tr = trace();
    return m_bcast.get_period();
  }

  auto
  set_bcast_period(std::optional<std::chrono::seconds> const& period)
    -> std::optional<std::chrono::seconds> override {
    auto tr = trace();
    return m_bcast.set_period(period);
  }

private:
  void
  post_recv(std::size_t n) {

    // To avoid a race condition this should only be called from the
    // DataCapture main loop, or when that loop is not running
    auto tr = trace();
    while (n-- > 0) {
      auto packet = this->packet_allocator().allocate(1);
      // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast,cppcoreguidelines-pro-bounds-pointer-arithmetic)
      char* buffer = reinterpret_cast<char*>(packet) - m_packet_offset;
      auto buf = std::span(buffer, m_packet_offset + sizeof(packet_type));
      if (m_packet_contexts.empty())
        m_packet_contexts.push(
          new std::remove_pointer_t<
            typename decltype(m_packet_contexts)::value_type>);
      auto ctx = m_packet_contexts.top();
      m_packet_contexts.pop();
      ctx->packet = packet;
      m_endpoint.ep->recv(
        buf,
        (m_endpoint.mr ? m_endpoint.mr->desc() : nullptr),
        FI_ADDR_UNSPEC,
        ctx);
    }
  }
};

template <
  IsFPacket P,
  IsFBlock B,
  unsigned MaxNumPacketOffsets,
  template <typename, typename>
  typename FillImpl>
class DataCaptureOFIFactory : public DataCaptureOFIUtil<P> {

  static auto
  check_context(oficpp::FiInfo const& info, decltype(fi_info::mode) const& ctx)
    -> bool {

    auto tr = trace();

    return (((info->rx_attr->mode & ctx) != 0) || ((info->mode & ctx) != 0));
  }

public:
  using data_capture_type =
    DataCaptureOFIBase<P, B, MaxNumPacketOffsets, FillImpl>;

  template <HasSingleBlockMemoryResource M>
  static auto
  create(
    std::shared_ptr<M> const& sbmr,
    std::size_t cq_size,
    oficpp::FiInfo const& info,
    std::string_view const& multicast_addr,
    std::set<channel_type> const& packet_channel_offsets,
    timestep_offset_type num_ts,
    std::optional<SweepParameters> const& sweep = std::nullopt,
    unsigned max_packet_arrival_skew_factor = 1,
    std::optional<std::chrono::seconds> const& bcast_period = std::nullopt)
    -> std::unique_ptr<data_capture_type> {

    auto tr = trace();
    if (check_context(info, FI_CONTEXT2))
      return std::make_unique<
        DataCaptureOFI<P, B, MaxNumPacketOffsets, FillImpl, 2>>(
        sbmr,
        cq_size,
        info,
        multicast_addr,
        packet_channel_offsets,
        num_ts,
        sweep,
        max_packet_arrival_skew_factor,
        bcast_period);
    if (check_context(info, FI_CONTEXT))
      return std::make_unique<
        DataCaptureOFI<P, B, MaxNumPacketOffsets, FillImpl, 1>>(
        sbmr,
        cq_size,
        info,
        multicast_addr,
        packet_channel_offsets,
        num_ts,
        sweep,
        max_packet_arrival_skew_factor,
        bcast_period);
    return std::make_unique<
      DataCaptureOFI<P, B, MaxNumPacketOffsets, FillImpl, 0>>(
      sbmr,
      cq_size,
      info,
      multicast_addr,
      packet_channel_offsets,
      num_ts,
      sweep,
      max_packet_arrival_skew_factor,
      bcast_period);
  }
};

} // end namespace rcp::dc

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
