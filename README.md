# rcpdc

Data capture library for radio camera processor

## Build guidance

- Minimum [CMake](https://cmake.org) version: 3.23
  - options: `Rcpdc_ENABLE_OFI` determines whether to use `libfabric`
    (default is `ON`)
- C++ standard: 20
  - tested with [gcc](https://gcc.gnu.org) v12.1.0
- Dependencies
  - [bark](https://gitlab.com/dsa-2000/rcp/bark), minimum version
    0.103.0
  - [librcp](https://gitlab.com/dsa-2000/rcp/librcp), minimum
    version 0.107.0
  - [oficpp](https://gitlab.com/dsa-2000/rcp/oficpp), when
    `Rcpdc_ENABLE_OFI` is set, minimum version 0.103.0
    (`Rcpdc_ENABLE_OFI=OFF` is broken)

## Spack package

[Spack](https://spack.io) package is available in the DSA-2000 Spack
[package repository](https://gitlab.com/dsa-2000/code/spack).

## Getting involved

The project web site may be found at
https://gitlab.com/dsa-2000/rcp/rcpdc. Instructions for cloning
the code repository can be found on that site. To provide feedback
(*e.g* bug reports or feature requests), please see the issue tracker
on that site. If you wish to contribute to this project, please read
[CONTRIBUTING.md](CONTRIBUTING.md) for guidelines.
