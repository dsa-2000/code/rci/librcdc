# Contributing to rcpdc

First off, thanks for taking the time to contribute!:sparkling_heart:

All types of contributions are encouraged and valued. See the [Table
of Contents](#table-of-contents) for different ways to help and
details about how this project handles them. Please make sure to read
the relevant section before making your contribution. It will make it
a lot easier for us maintainers and smooth out the experience for all
involved. The community looks forward to your contributions.:tada:

## Table of Contents

- [I Have a Question](#i-have-a-question)
- [I Want to Contribute](#i-want-to-contribute)
- [Reporting Bugs](#reporting-bugs)
- [Suggesting Enhancements](#suggesting-enhancements)
- [Your First Code Contribution](#your-first-code-contribution)
- [Branch Strategy](#branch-strategy)
- [Improving the Documentation](#improving-the-documentation)
- [Style Guides](#style-guides)
- [Commit Messages](#commit-messages)
- [Join the Project Team](#join-the-project-team)



## I Have a Question

> If you want to ask a question, we assume that you have read the available [Documentation]().

Before you ask a question, it is best to search for existing
[Issues](https://gitlab.com/dsa-2000/rcp/rcpdc/-/issues) that
might help you. In case you have found a suitable issue and still need
clarification, you can write your question in this issue.

If you then still feel the need to ask a question and need
clarification, we recommend the following:

- Open an [Issue](https://gitlab.com/dsa-2000/rcp/rcpdc/-/issues/new).
- Provide as much context as you can about what you're running into.
- Provide project and platform versions (g++, CUDA, *etc*), depending
  on what seems relevant.
- Mark the issue with the `question` tag.

We will then take care of the issue as soon as possible.

## I Want to Contribute

> ### Legal Notice
> When contributing to this project, you must agree that you have
> authored 100% of the content, that you have the necessary rights to
> the content and that the content you contribute may be provided
> under the project license.

### Reporting Bugs

#### Before Submitting a Bug Report

A good bug report shouldn't leave others needing to chase you up for
more information. Therefore, we ask you to investigate carefully,
collect information and describe the issue in detail in your
report. Please complete the following steps in advance to help us fix
any potential bug as fast as possible.

- Make sure that you are using the latest version. If that is not
  possible for you, indicate clearly the version number you are using.
- Determine if your bug is really a bug and not an error on your side
  *e.g* using incompatible environment components/versions (Make sure
  that you have read the [documentation](). If you are looking for
  support, you might want to check [this
  section](#i-have-a-question)).
- To see if other users have experienced (and potentially already
  solved) the same issue you are having, check if there is not already
  an existing bug report for your bug or error in the [bug
  tracker](https://gitlab.com/dsa-2000/rcp/rcpdc/-/issues?state=opened&label_name[]=bug).
- Collect information about the bug:
  - Stack trace (traceback)
  - OS, Platform and Version (Windows, Linux, macOS, x86, ARM)
  - Version of the interpreter, compiler, SDK, runtime environment,
    package manager, depending on what seems relevant.
  - Possibly your input and the output
  - Can you reliably reproduce the issue? And can you also reproduce it
    with older versions?

#### How Do I Submit a Good Bug Report?

> You must never report security related issues, vulnerabilities or
> bugs including sensitive information to the issue tracker, or
> elsewhere in public. Instead sensitive bugs must be sent by email,
> preferably encrypted, to [@martin_pokorny](mailto:mpokorny at caltech dot edu).

We use GitLab issues to track bugs and errors. If you run into an
issue with the project:

- Open an
  [Issue](https://gitlab.com/dsa-2000/rcp/rcpdc/-/issues/new). (Since
  we can't be sure at this point whether it is a bug or not, we ask
  you not to talk about the issue as a bug yet, and not to label it as
  such.)
- Explain the behavior you would expect and the actual behavior.
- Please provide as much context as possible and describe the
  *reproduction steps* that someone else can follow to recreate the
  issue on their own. This usually includes your code. For good bug
  reports you should isolate the problem and create a reduced test
  case whenever possible.
- Provide the information you collected in the previous section.

Once it's filed:

- The project team will label the issue accordingly.
- A team member will try to reproduce the issue with your provided
  steps. If there are no reproduction steps or no obvious way to
  reproduce the issue, the team will ask you for those steps and mark
  the issue as `needs input`. Bugs with the `needs input` tag will not
  be addressed until they are reproduced.
- If the team is able to reproduce the issue, it will be marked
  `confirmed`, as well as possibly other tags (such as `critical`),
  and the issue will be left to be [implemented by
  someone](#your-first-code-contribution).

### Suggesting Enhancements and Other Changes

This section guides you through submitting an enhancement suggestion
for `rcpdc`, **including completely new features and minor improvements
to existing functionality**. Following these guidelines will help
maintainers and the community to understand your suggestion and find
related suggestions.

#### Before Submitting an Enhancement

- Make sure that you are using the latest version.
- Read the [documentation]() carefully and find out if the
  functionality is already covered, maybe by a configuration you
  haven't previously considered.
- Perform a
  [search](https://gitlab.com/dsa-2000/rcp/rcpdc/-/issues)
  to see if the enhancement has already been suggested. If it has, add
  a comment to the existing issue instead of opening a new one.
- Find out whether your idea fits with the scope and aims of the
  project. It's up to you to make a strong case to convince the
  project's developers of the merits of this feature. Keep in mind
  that we want features that will be useful to the majority of our
  users and not just a small subset. If you're just targeting a
  minority of users, consider writing an add-on/plugin library.

#### How Do I Submit a Good Enhancement Suggestion?

Enhancement suggestions are tracked as [GitLab
issues](https://gitlab.com/dsa-2000/rcp/rcpdc/-/issues).

- Use a **clear and descriptive title** for the issue to identify the suggestion.
- Provide a **step-by-step description of the suggested enhancement**
  in as many details as possible.
- **Describe the current behavior** and **explain which behavior you
  expected to see instead** and why. At this point you can also tell
  which alternatives do not work for you.
- You may want to **include screenshots and animated GIFs** which help
  you demonstrate the steps or point out the part which the suggestion
  is related to. You can use [this
  tool](https://www.cockos.com/licecap/) to record GIFs on macOS and
  Windows, and [this tool](https://github.com/colinkeenan/silentcast)
  or [this tool](https://github.com/GNOME/byzanz) on Linux. <!-- this
  should only be included if the project has a GUI -->
- **Explain why this enhancement would be useful** to most `rcpdc`
  users. You may also want to point out the other projects that have
  similar features, and which could serve as inspiration.
- If your suggestion is concrete, consider adding the `enhancement`
  tag to the issue. On the other hand, if your suggestion is intended
  more to initiate a discussion, you may use the `discussion` tag
  instead.

### Your First Code Contribution

Code contributions are expected to be implemented on a branch you have
created. Note that all commits in your branch must be
cryptographically signed[^gpg]. After you have made the changes to
your branch, have created and/or modified unit tests for all new or
modified functionality, have verified that all unit tests are passing
in your development environment, and have verified that the code meets
all the style guidelines of the project, you are ready to submit a
merge request.  Create a GitLab [merge
request](https://gitlab.com/dsa-2000/rcp/rcpdc/-/merge_requests/new)
on the project web site.

[^gpg]: Cryptographic key use and management is a complex topic, but
    there are many resources available on the web. To get you started,
    have a look at [this
    tutorial](https://www.digitalocean.com/community/tutorials/how-to-use-gpg-to-encrypt-and-sign-messages). Don't
    be afraid to ask a project maintainer for help, should you be
    having any difficulty: we would much rather see your contributions
    than lose a new contributor.

Some things to keep in mind:
- When a new merge request is created, it marks the start of a
  conversation about the changes in the merge request, in which you
  are expected to be an active participant.
- All merge requests will be reviewed before they are accepted, and
  you may be asked to make modifications by one or more reviewers.
- Don't be afraid to rebase the commits in a branch for which you are
  the sole contributor. In fact, it is strongly recommended that you
  do so to keep the commit history in your branch simple and clean
  prior to the merge.

### Branch Strategy

This project follows a [trunk based
development](https://trunkbaseddevelopment.com/) methodology. For
development on a branch, you should keep your branches tightly focused
and small. Merges that break the build will be reverted as quickly as
possible. Project team members may commit directly to `main` in some
cases, but that is not allowed for non-members.

### Improving the Documentation

The bulk of documentation in this project is in the form of source
code comments, using the Doxygen format. Modifications to the source
code documentation, or the CI pipeline that renders the source code
documentation on the project's documentation site, should be done
using a merge request as described above.

Further project documentation, such as HOWTOs, FAQs, recipes, *etc*
can be created on the project's [wiki
pages](https://gitlab.com/dsa-2000/rcp/rcpdc/-/wikis/home). If
you want to contribute to that documentation, you are urged to
coordinate with the project maintainers before making changes to the
wiki, preferably by opening an
[issue](https://gitlab.com/dsa-2000/rcp/rcpdc/-/issues/new)
with the `documentation` tag.

## Style Guides

### Code styles

As the programming language used by this project is primarily C++, we
have adopted the use of the `clang` tools `clang-tidy` and
`clang-format` for managing code style. In the repository you will
find both `.clang-tidy` and `.clang-format` files that define the
style guidelines adopted by this project. Unfortunately, because a CI
pipeline to automatically check the code style has not been
implemented, it is required that developers use the clang tools with
these configuration files to check and correct code style before
making a merge request.

### Commit Messages

Commit messages should follow well-established guidelines, which are
widely available on the web. See https://cbea.ms/git-commit/ for one
such guide. The key points are

1. Separate subject from body with a blank line
1. Limit the subject line to 50 characters
1. Capitalize the subject line
1. Do not end the subject line with a period
1. Use the imperative mood in the subject line
1. Wrap the body at 72 characters
1. Use the body to explain **what** and **why** *vs* **how**


## Join the Project Team

If you are an active user and/or developer of this project, we would
be excited to have you join our the project team.:sparkles: Please
reach out to [@martin_pokorny](mailto:mpokorny at caltech dot edu) if
you are interested.
