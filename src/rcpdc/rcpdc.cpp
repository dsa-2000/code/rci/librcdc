// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "rcp/dc/rcpdc.hpp"

using namespace rcp::dc;

// NOLINTBEGIN(cppcoreguidelines-avoid-non-const-global-variables)
rcp::LogSink<Tag> rcp::dc::log_rcpdc;
// NOLINTEND(cppcoreguidelines-avoid-non-const-global-variables)

template <>
auto
rcp::get_log_sink<rcp::dc::Tag>() -> LogSink<rcp::dc::Tag>& {
  return log_rcpdc;
}

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
