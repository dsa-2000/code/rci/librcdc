// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "rcp/dc/BlockFiller.hpp"
#include "rcp/dc/rcpdc.hpp"
#include "gtest/gtest.h"

#include <rcp/DataPacketConfiguration.hpp>

#include <array>
#include <experimental/mdspan>
#include <memory_resource>
#include <vector>

std::size_t constexpr block_size = 2 << 20;
std::array<char, block_size> block{};
std::pmr::monotonic_buffer_resource
  buffer_block(block.data(), block.size(), std::pmr::null_memory_resource());
std::pmr::unsynchronized_pool_resource pool(&buffer_block);

template <rcp::dc::IsFPacket P>
void
init_packet(
  P* packet,
  rcp::dc::receiver_type receiver,
  rcp::dc::timestep_offset_type ts,
  rcp::dc::channel_type channel) {
  packet->receiver = receiver;
  packet->timestamp_offset = ts * P::sample_interval_ns;
  packet->channel_offset = channel;
  auto fsamples = std::experimental::mdspan(
    packet->fsamples.data(), typename P::mapping_type{});
  for (auto&& ch : P::channels())
    for (auto&& pol : P::polarizations())
      for (auto&& ts : P::timesteps())
        fsamples(ch, pol, ts) = rcp::dc::fsample_type{59};
}

template <rcp::dc::IsFPacket P, rcp::dc::IsFBlock B>
struct FillImpl {

  using pkt_mdspan_type = decltype(std::declval<P>().fsamples_mdspan());
  using bsmp_mdspan_type = decltype(std::declval<B>().samples_mdspan());
  using bwgt_mdspan_type = decltype(std::declval<B>().weights_mdspan());
  using size_type = typename pkt_mdspan_type::size_type;

  static_assert(P::timesteps_tile_size == B::timesteps_tile_size);

  static void
  copy_packet_to_block(
    size_type num_ts,
    pkt_mdspan_type const& packet,
    size_type pkt_ts0,
    bsmp_mdspan_type const& samples,
    bwgt_mdspan_type const& weights,
    size_type receiver,
    size_type blk_ts0) {

    for (auto&& ch : std::views::iota(size_type{0}, packet.extent(0)))
      for (auto&& pol : std::views::iota(size_type{0}, packet.extent(1))) {
        for (size_type ts_offset = 0; ts_offset < num_ts;
             ts_offset += size_type{P::timesteps_tile_size}) {
          auto pkt = &packet(ch, pol, pkt_ts0 + ts_offset);
          auto smp = &samples(ch, receiver, pol, blk_ts0 + ts_offset);
          auto wgt = &weights(ch, receiver, pol, blk_ts0 + ts_offset);
          for (auto&& ts : std::views::iota(
                 size_type{0}, size_type{P::timesteps_tile_size})) {
            if (!rcp::DataPacketConfiguration::is_flagged(pkt[ts])) {
              smp[ts] = pkt[ts];
              wgt[ts] = 1;
            } else {
              smp[ts] = rcp::dc::fsample_type{0};
              wgt[ts] = 0;
            }
          }
        }
      }
  }
};

using namespace std::chrono_literals;

template <typename B>
auto
make_block(
  rcp::dc::timestep_offset_type num_ts,
  rcp::dc::timestamp_type ts,
  rcp::dc::channel_type ch)
  -> std::tuple<
    std::shared_ptr<std::vector<rcp::dc::fsample_type>>,
    std::shared_ptr<std::vector<rcp::dc::fweight_type>>,
    std::shared_ptr<B>> {

  using extents_type = typename B::extents_type;
  using mapping_type = typename B::mapping_type;
  auto samples = std::make_shared<std::vector<rcp::dc::fsample_type>>(
    mapping_type{extents_type{num_ts}}.required_span_size());
  auto weights = std::make_shared<std::vector<rcp::dc::fweight_type>>(
    mapping_type{extents_type{num_ts}}.required_span_size());
  auto blk = std::make_shared<B>(
    ch, ts, num_ts, std::span(*samples), std::span(*weights));
  return {std::move(samples), std::move(weights), std::move(blk)};
}

TEST(BlockFiller, Fill) {
  constexpr unsigned num_channels = 1;
  constexpr unsigned num_polarizations = 2;
  constexpr unsigned num_timesteps_per_packet = 1;
  constexpr unsigned num_receivers = 3;
  using fpacket_t = rcp::dc::FPacket<
    10'000,
    num_channels,
    num_polarizations,
    num_timesteps_per_packet,
    num_timesteps_per_packet>;
  using fblock_t = rcp::dc::FBlock<
    10'000,
    num_receivers,
    fpacket_t::num_channels,
    fpacket_t::num_polarizations,
    fpacket_t::num_timesteps>;
  std::pmr::polymorphic_allocator<fpacket_t> pktalloc(&pool);
  using bf_t =
    rcp::dc::BlockFiller<fpacket_t, fblock_t, FillImpl<fpacket_t, fblock_t>>;
  rcp::get_log_sink<rcp::dc::Tag>().min_level() = bark::Level::Warn;

  // simple tests of block filling: num_receivers packets in 1 block
  {
    bf_t bf(1);
    auto const packets_per_blk =
      fblock_t::size(fpacket_t::num_timesteps) / fpacket_t::size;
    // std::size_t const sz0 = packets_per_blk * sizeof(fpacket_t);
    std::vector<fpacket_t*> ps;
    for (std::size_t i = 0; i < packets_per_blk; ++i) {
      ps.emplace_back(pktalloc.allocate(1));
      init_packet(ps.back(), i, 0, 0);
    }
    {
      auto [samples, weights, blk] =
        make_block<fblock_t>(fpacket_t::num_timesteps, 0, 0);
      auto pblk = std::promise<bf_t::fill_result_t>();
      auto fblk = pblk.get_future();
      bf.fill(blk, std::move(pblk));
      bf.process_packets(0, ps);
      EXPECT_EQ(fblk.wait_for(1ms), std::future_status::ready);
      EXPECT_EQ(blk->timestamp_offset, 0);
      EXPECT_EQ(blk->channel_offset, 0);
    }
  }
  // similar test, using callback for completion
  {
    bf_t bf(1);
    auto const packets_per_blk =
      fblock_t::size(fpacket_t::num_timesteps) / fpacket_t::size;
    // std::size_t const sz0 = packets_per_blk * sizeof(fpacket_t);
    std::vector<fpacket_t*> ps;
    for (std::size_t i = 0; i < packets_per_blk; ++i) {
      ps.emplace_back(pktalloc.allocate(1));
      init_packet(ps.back(), i, 0, 0);
    }
    {
      auto [samples, weights, blk] =
        make_block<fblock_t>(fpacket_t::num_timesteps, 0, 0);
      auto pblk = std::promise<bf_t::fill_result_t>();
      bool done = false;
      auto filled_cb = [&](bf_t::fill_result_t const& /*fr*/) -> void {
        done = true;
      };
      bf.fill(blk, filled_cb);
      bf.process_packets(0, ps);
      EXPECT_TRUE(done);
      EXPECT_EQ(blk->timestamp_offset, 0);
      EXPECT_EQ(blk->channel_offset, 0);
    }
  }

  // test that futures are completed after destruction of BlockFiller instance
  {
    auto [samples, weights, blk] =
      make_block<fblock_t>(fpacket_t::num_timesteps, 0, 0);
    auto pblk = std::promise<bf_t::fill_result_t>();
    auto fblk = pblk.get_future();
    {
      bf_t bf(1);
      bf.fill(blk, std::move(pblk));
    }
    EXPECT_EQ(fblk.wait_for(1ms), std::future_status::ready);
  }

  // test that callbacks are invoked after destruction of BlockFiller instance
  {
    auto [samples, weights, blk] =
      make_block<fblock_t>(fpacket_t::num_timesteps, 0, 0);
    bool done = false;
    auto filled_cb = [&](bf_t::fill_result_t const& /*fr*/) -> void {
      done = true;
    };
    {
      bf_t bf(1);
      bf.fill(blk, filled_cb);
    }
    EXPECT_TRUE(done);
  }

  // tests of block filling with 2 * num_receivers packets, 2 timestamps (out of
  // order) in 2 blocks
  {
    bf_t bf(1);
    auto const packets_per_blk =
      fblock_t::size(fpacket_t::num_timesteps) / fpacket_t::size;
    std::vector<fpacket_t*> ps;
    for (std::size_t i = 0; i < 2 * packets_per_blk; ++i) {
      ps.emplace_back(pktalloc.allocate(1));
      init_packet(ps.back(), i / 2, i % 2, 0);
    }
    {
      auto [samples0, weights0, blk0] =
        make_block<fblock_t>(fpacket_t::num_timesteps, 0, 0);
      auto pblk0 = std::promise<bf_t::fill_result_t>();
      auto fblk0 = pblk0.get_future();
      bf.fill(blk0, std::move(pblk0));
      auto [samples1, weights1, blk1] = make_block<fblock_t>(
        fpacket_t::num_timesteps, 1 * fpacket_t::sample_interval_ns, 0);
      auto pblk1 = std::promise<bf_t::fill_result_t>();
      auto fblk1 = pblk1.get_future();
      bf.fill(blk1, std::move(pblk1));
      bf.process_packets(0, ps);
      bf.flush();
      EXPECT_EQ(fblk0.wait_for(1ms), std::future_status::ready);
      EXPECT_EQ(blk0->timestamp_offset, 0);
      EXPECT_EQ(blk0->channel_offset, 0);
      EXPECT_EQ(fblk1.wait_for(1ms), std::future_status::ready);
      EXPECT_EQ(blk1->timestamp_offset, 1 * fpacket_t::sample_interval_ns);
      EXPECT_EQ(blk1->channel_offset, 0);
    }
  }

  // some tests involving submitting packets prior to start, and also after
  // flush
  {
    bf_t bf(1);
    auto const packets_per_blk =
      fblock_t::size(fpacket_t::num_timesteps) / fpacket_t::size;
    {
      std::vector<fpacket_t*> ps;

      for (std::size_t i = 0; i < 2 * packets_per_blk; ++i) {
        ps.emplace_back(pktalloc.allocate(1));
        init_packet(ps.back(), i / 2, i % 2 + 2, 0);
      }
      {
        auto [samples0, weights0, blk0] = make_block<fblock_t>(
          fpacket_t::num_timesteps, 2 * fpacket_t::sample_interval_ns, 0);
        auto pblk0 = std::promise<bf_t::fill_result_t>();
        auto fblk0 = pblk0.get_future();
        bf.fill(blk0, std::move(pblk0));
        auto [samples1, weights1, blk1] = make_block<fblock_t>(
          fpacket_t::num_timesteps, 3 * fpacket_t::sample_interval_ns, 0);
        auto pblk1 = std::promise<bf_t::fill_result_t>();
        auto fblk1 = pblk1.get_future();
        bf.fill(blk1, std::move(pblk1));
        bf.process_packets(2 * fpacket_t::sample_interval_ns, ps);
        bf.flush();
        EXPECT_EQ(fblk0.wait_for(1ms), std::future_status::ready);
        EXPECT_EQ(blk0->timestamp_offset, 2 * fpacket_t::sample_interval_ns);
        EXPECT_EQ(blk0->channel_offset, 0);
        EXPECT_EQ(fblk1.wait_for(1ms), std::future_status::ready);
        EXPECT_EQ(blk1->timestamp_offset, 3 * fpacket_t::sample_interval_ns);
        EXPECT_EQ(blk1->channel_offset, 0);
      }
      for (std::size_t i = 0; i < 2 * packets_per_blk; ++i) {
        ps.emplace_back(pktalloc.allocate(1));
        init_packet(ps.back(), i / 2, i % 2 + 4, 0);
      }
      {
        auto [samples0, weights0, blk0] = make_block<fblock_t>(
          fpacket_t::num_timesteps, 4 * fpacket_t::sample_interval_ns, 0);
        auto pblk0 = std::promise<bf_t::fill_result_t>();
        auto fblk0 = pblk0.get_future();
        bf.fill(blk0, std::move(pblk0));
        auto [samples1, weights1, blk1] = make_block<fblock_t>(
          fpacket_t::num_timesteps, 5 * fpacket_t::sample_interval_ns, 0);
        auto pblk1 = std::promise<bf_t::fill_result_t>();
        auto fblk1 = pblk1.get_future();
        bf.fill(blk1, std::move(pblk1));
        bf.process_packets(2 * fpacket_t::sample_interval_ns, ps);
        EXPECT_EQ(fblk0.wait_for(1ms), std::future_status::ready);
        EXPECT_EQ(blk0->timestamp_offset, 4 * fpacket_t::sample_interval_ns);
        EXPECT_EQ(blk0->channel_offset, 0);
        EXPECT_EQ(fblk1.wait_for(1ms), std::future_status::ready);
        EXPECT_EQ(blk1->timestamp_offset, 5 * fpacket_t::sample_interval_ns);
        EXPECT_EQ(blk1->channel_offset, 0);
      }
    }
  }
}

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
