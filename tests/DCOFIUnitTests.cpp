// Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
// SPDX-License-Identifier: BSD-2-Clause-Patent
#include "rcp/dc/DataCaptureOFI.hpp"
#include "rcp/dc/rcpdc.hpp"
#include "gtest/gtest.h"

#include <rcp/DataPacketConfiguration.hpp>

#include <experimental/mdspan>
#include <vector>

std::vector<char> block(2 << 20);
rcp::dc::single_block_memory_resource buffer_block(block.data(), block.size());

struct tracking_resource : public std::pmr::memory_resource {

  static constexpr bool has_single_block_memory_resource = true;

  std::pmr::memory_resource* m_base_mr;

  void* m_address;

  std::size_t m_size;

  std::size_t m_allocate_size;

  std::shared_ptr<std::mutex> m_mtx;

  std::map<void*, std::size_t> m_allocated;

  template <rcp::dc::HasSingleBlockMemoryResource M>
  tracking_resource(M* mr)
    : m_base_mr(mr)
    , m_address(mr->address())
    , m_size(mr->size())
    , m_allocate_size(0)
    , m_mtx(std::make_shared<std::mutex>()) {}

  tracking_resource(const tracking_resource&) = delete;

  tracking_resource(tracking_resource&&) = default;

  auto
  operator=(const tracking_resource&) -> tracking_resource& = delete;

  auto
  operator=(tracking_resource&&) -> tracking_resource& = default;

  auto
  allocate_size() {
    std::scoped_lock lk(*m_mtx);
    return m_allocate_size;
  }

  auto
  do_allocate(std::size_t bytes, std::size_t alignment) -> void* override {
    std::scoped_lock lk(*m_mtx);
    auto result = m_base_mr->allocate(bytes, alignment);
    m_allocated[result] = bytes;
    m_allocate_size += bytes;
    return result;
  }

  void
  do_deallocate(void* p, std::size_t bytes, std::size_t alignment) override {
    std::scoped_lock lk(*m_mtx);
    if (!m_allocated.contains(p))
      throw std::runtime_error("deallocating unallocated block");
    m_allocated.erase(p);
    m_base_mr->deallocate(p, bytes, alignment);
    m_allocate_size -= bytes;
  }

  auto
  do_is_equal(std::pmr::memory_resource const& other) const noexcept
    -> bool override {
    try {
      auto const& other_a = dynamic_cast<tracking_resource const&>(other);
      return m_base_mr->is_equal(*other_a.m_base_mr);
    } catch (std::bad_cast const&) {
      return false;
    }
  }

  bool
  is_allocation_start(void* p) {
    return m_allocated.contains(p);
  }

  auto
  address() -> void* {
    return m_address;
  }

  auto
  size() const -> std::size_t {
    return m_size;
  }
};

template <rcp::dc::IsFPacket P>
void
init_packet(
  P* packet,
  rcp::dc::receiver_type receiver,
  rcp::dc::timestep_offset_type ts,
  rcp::dc::channel_type channel) {
  packet->receiver = receiver;
  packet->timestamp_offset = ts * P::sample_interval_ns;
  packet->channel_offset = channel;
  auto fsamples =
    std::experimental::mdspan(packet->fsamples, typename P::mapping_type{});
  for (auto&& ch : P::channels())
    for (auto&& pol : P::polarizations())
      for (auto&& ts : P::timesteps())
        fsamples(ch, pol, ts) = rcp::dc::fsample_type{59};
}

using namespace std::chrono_literals;

template <typename B>
auto
make_block(
  rcp::dc::timestep_offset_type num_ts,
  rcp::dc::timestamp_type ts,
  rcp::dc::channel_type ch)
  -> std::tuple<
    std::shared_ptr<std::vector<rcp::dc::fsample_type>>,
    std::shared_ptr<std::vector<rcp::dc::fweight_type>>,
    std::shared_ptr<B>> {

  using extents_type = typename B::extents_type;
  using mapping_type = typename B::mapping_type;
  auto samples = std::make_shared<std::vector<rcp::dc::fsample_type>>(
    mapping_type{extents_type{num_ts}}.required_span_size());
  auto weights = std::make_shared<std::vector<rcp::dc::fweight_type>>(
    mapping_type{extents_type{num_ts}}.required_span_size());
  auto blk = std::make_shared<B>(
    ch, ts, num_ts, std::span(*samples), std::span(*weights));
  return {std::move(samples), std::move(weights), std::move(blk)};
}

template <rcp::dc::IsFPacket P, rcp::dc::IsFBlock B>
struct FillImpl {

  using pkt_mdspan_type = decltype(std::declval<P>().fsamples_mdspan());
  using bsmp_mdspan_type = decltype(std::declval<B>().samples_mdspan());
  using bwgt_mdspan_type = decltype(std::declval<B>().weights_mdspan());
  using size_type = typename pkt_mdspan_type::size_type;

  static_assert(P::timesteps_tile_size == B::timesteps_tile_size);

  static void
  copy_packet_to_block(
    size_type num_ts,
    pkt_mdspan_type const& packet,
    size_type pkt_ts0,
    bsmp_mdspan_type const& samples,
    bwgt_mdspan_type const& weights,
    size_type receiver,
    size_type blk_ts0) {

    for (auto&& ch : std::views::iota(size_type{0}, packet.extent(0)))
      for (auto&& pol : std::views::iota(size_type{0}, packet.extent(1))) {
        for (size_type ts_offset = 0; ts_offset < num_ts;
             ts_offset += size_type{P::timesteps_tile_size}) {
          auto pkt = &packet(ch, pol, pkt_ts0 + ts_offset);
          auto smp = &samples(ch, receiver, pol, blk_ts0 + ts_offset);
          auto wgt = &weights(ch, receiver, pol, blk_ts0 + ts_offset);
          for (auto&& ts : std::views::iota(
                 size_type{0}, size_type{P::timesteps_tile_size})) {
            if (!rcp::DataPacketConfiguration::is_flagged(pkt[ts])) {
              smp[ts] = pkt[ts];
              wgt[ts] = 1;
            } else {
              smp[ts] = rcp::dc::fsample_type{0};
              wgt[ts] = 0;
            }
          }
        }
      }
  }
};

TEST(DataCaptureOFI, prefixed_single_block_pool) {
  std::size_t const prefix_sz = 16;
  tracking_resource tr{&buffer_block};
  EXPECT_EQ(tr.allocate_size(), 0);
  rcp::dc::prefixed_single_block_pool psbp(prefix_sz, &tr);
  EXPECT_EQ(tr.allocate_size(), 0);
  auto int_allocator = std::pmr::polymorphic_allocator<int>(&psbp);

  unsigned nx = 1;
  auto x = int_allocator.allocate(nx);
  EXPECT_EQ(tr.allocate_size(), nx * sizeof(*x) + prefix_sz);
  EXPECT_FALSE(tr.is_allocation_start(x));
  EXPECT_TRUE(tr.is_allocation_start(reinterpret_cast<char*>(x) - prefix_sz));
  int_allocator.deallocate(x, nx);
  EXPECT_EQ(tr.allocate_size(), 0);

  unsigned ny = 2;
  auto y = int_allocator.allocate(ny);
  auto szy = ny * sizeof(*y) + prefix_sz;
  EXPECT_EQ(tr.allocate_size(), szy);
  EXPECT_FALSE(tr.is_allocation_start(y));
  EXPECT_TRUE(tr.is_allocation_start(reinterpret_cast<char*>(y) - prefix_sz));

  unsigned nz = 3;
  auto z = int_allocator.allocate(nz);
  auto szz = nz * sizeof(*z) + prefix_sz;
  EXPECT_EQ(tr.allocate_size(), szy + szz);
  EXPECT_FALSE(tr.is_allocation_start(z));
  EXPECT_TRUE(tr.is_allocation_start(reinterpret_cast<char*>(z) - prefix_sz));

  int_allocator.deallocate(y, ny);
  EXPECT_EQ(tr.allocate_size(), szz);
  int_allocator.deallocate(z, nz);
  EXPECT_EQ(tr.allocate_size(), 0);
}

TEST(DataCaptureOFI, DataCaptureOFI) {
  constexpr unsigned num_channels = 1;
  constexpr unsigned num_polarizations = 2;
  constexpr unsigned num_timesteps_per_packet = 1;
  constexpr unsigned num_receivers = 3;
  using fpacket_t = rcp::dc::FPacket<
    10'000,
    num_channels,
    num_polarizations,
    num_timesteps_per_packet,
    num_timesteps_per_packet>;
  using fblock_t = rcp::dc::FBlock<
    10'000,
    num_receivers,
    fpacket_t::num_channels,
    fpacket_t::num_polarizations,
    fpacket_t::num_timesteps>;
  auto mr =
    std::make_shared<rcp::dc::single_block_synchronized_pool>(&buffer_block);
  // auto const packets_per_blk =
  //   fblock_t::size(fpacket_t::num_timesteps) / fpacket_t::size;
  using factory_t =
    rcp::dc::DataCaptureOFIFactory<fpacket_t, fblock_t, 4, FillImpl>;
  {
    auto infos = factory_t::find_fabric_interfaces("udp");
    ASSERT_GE(infos.size(), 1);
    auto msg_size = factory_t::rx_message_size(infos.front());
    auto dc = factory_t::create(
      mr,
      mr->size() / 10 / msg_size,
      infos.front(),
      "224.0.0.1:5000",
      {0},
      fpacket_t::num_timesteps);
    EXPECT_FALSE(dc->get_bcast_period());

    std::pmr::polymorphic_allocator<fpacket_t> pktalloc(
      dc->packet_memory_resource().get());

    EXPECT_FALSE(dc->epoch());
    dc->start();
    EXPECT_FALSE(dc->epoch());

    auto [samples1, weights1, blk1] = make_block<fblock_t>(
      fpacket_t::num_timesteps, 1 * fpacket_t::sample_interval_ns, 0);
    auto fblk1 = dc->fill(blk1);
    std::this_thread::sleep_for(1ms);
    ASSERT_TRUE(dc->epoch());
    EXPECT_EQ(*dc->epoch(), 1 * fpacket_t::sample_interval_ns);

    auto [samples2, weights2, blk2] = make_block<fblock_t>(
      fpacket_t::num_timesteps, 2 * fpacket_t::sample_interval_ns, 0);
    auto fblk2 = dc->fill(blk2);
    std::this_thread::sleep_for(1ms);
    ASSERT_TRUE(dc->epoch());
    EXPECT_EQ(*dc->epoch(), 1 * fpacket_t::sample_interval_ns);

    EXPECT_EQ(fblk1.wait_for(10ms), std::future_status::timeout);
    EXPECT_EQ(fblk2.wait_for(10ms), std::future_status::timeout);

    // TODO: to make the rest of these tests work, we need a way to inject
    // packets so that blocks can be completed...unlikely to happen

    // std::vector<fpacket_t*> ps;
    // for (std::size_t i = 0; i < 2 * packets_per_blk; ++i) {
    //   ps.emplace_back(pktalloc.allocate(1));
    //   init_packet(ps.back(), i / 2, i % 2 + 1, 0);
    // }

    // std::this_thread::sleep_for(5s);
    // //dc.reload({{std::move(ps)}});
    // ASSERT_EQ(fblk1.wait_for(10ms), std::future_status::ready);
    // auto [n1, f1] = fblk1.get();
    // EXPECT_EQ(f1, blk1);
    // EXPECT_EQ(n1, f1->size());
    // ASSERT_EQ(fblk2.wait_for(10ms), std::future_status::ready);
    // auto [n2, f2] = fblk2.get();
    // EXPECT_EQ(f2, blk2);
    // EXPECT_EQ(n2, f2->size());
    std::this_thread::sleep_for(5ms);

    dc->stop();
  }
}

// Local Variables:
// mode: c++
// c-basic-offset: 2
// fill-column: 80
// indent-tabs-mode: nil
// flycheck-gcc-language-standard: "c++20"
// End:
