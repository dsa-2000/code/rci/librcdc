# Copyright 2022-2023 Martin Pokorny <mpokorny@caltech.edu>
# SPDX-License-Identifier: BSD-2-Clause-Patent
#
# This file should contain nothing but the following line setting the
# project version. The variable name must not clash with the
# aq_VERSION* variables automatically defined by the project()
# command.
#
# This supports aligning a git commit to each change in version
# number, and also allows parent projects to provide their own version
# number.
set(Rcpdc_VER 0.106.0)
