## 0.106.0
 (2024-11-05)

### Feature changes (2 changes)

- [Update librcp dependency to 0.107.0](https://gitlab.com/dsa-2000/rcp/rcpdc/-/commit/67f2f0f4518378a776d38f74d438591a456b60d9)
- [Replace use of flagged_fsample with is_flagged()](https://gitlab.com/dsa-2000/rcp/rcpdc/-/commit/0b14469c00e1ea6fc70ea4bba573101f14383706)

## 0.105.1
 (2024-09-06)

### Other (1 change)

- [Bump librcp dependency to v0.105.0](https://gitlab.com/dsa-2000/rcp/rcpdc/-/commit/0f220bb90782b54a72c5c2959dff4184b2deeda1)

## 0.105.0
 (2024-08-01)

### New features (1 change)

- [Enable UDP with sender multicast](https://gitlab.com/dsa-2000/rcp/rcpdc/-/commit/ac724a8459dcfc4f6d13a9ec332647641dba4630)

### Feature changes (2 changes)

- [Fix up some include statements](https://gitlab.com/dsa-2000/rcp/rcpdc/-/commit/99851e56e2f9eeb83c52a4384fb18f83546f2865)
- [Update oficpp dependency to v0.103.0](https://gitlab.com/dsa-2000/rcp/rcpdc/-/commit/cf1447b364f18c6f36b90a549fa62511933852e0)

## v0.104.0
 (2023-11-21)

### Other (1 change)

- [Update dependent library versions](dsa-2000/rcp/rcpdc@395309141cd3c66c4226628c7fd7fb3aca8f6980)

## v0.103.0
 (2023-11-16)

### Bug fixes (1 change)

- [Insert a missing "typename" flagged by clang-tidy](dsa-2000/code/rcp/rcpdc@e1babcc6c1f0fe1fd938f62ac5d186b77b0d50b9)

### Feature removals (1 change)

- [Remove TiledSamplesLayoutSave](dsa-2000/code/rcp/rcpdc@80f1a118cf41f7e51186f4df05e268d8f5b1f1ce)
## v0.102.0
 (2023-06-30)

### Feature changes (1 change)

- [Capitalize CMake target namespaces](dsa-2000/code/rcp/rcpdc@8f8d9748596793b27d3538d2ae049a710f05cb65)

### Other (1 change)

- [Update bark, librcp, oficpp dependency versions](dsa-2000/code/rcp/rcpdc@031c30f21d2e5c4e7d045f958283484254871fe8)

## v0.101.0
 (2023-06-12)

### Bug fixes (1 change)

- [Correct namespace for use of `dynamic_extent`](dsa-2000/code/rcp/rcpdc@7c664810974ad36657313829ec1a40e3aecdec03)

### Other (1 change)

- [Change librcp dependency version to 0.101.0](dsa-2000/code/rcp/rcpdc@271f8851564b2e5bff7cd7ab3091a4a9b9df6243)

## v0.100.0
 (2023-06-11)

### Other (1 change)

- Reset version to 0.100.0
